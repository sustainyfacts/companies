/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datasetmodelapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gopkg.in/yaml.v3"

	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/datasetmodel"
)

// This service can be used concurrently by multiple go-routines
var service datasetmodel.Service
var log = logger.Get("datasetmodel")

func Configure(s datasetmodel.Service, router chi.Router) {
	service = s
	router.Get("/datasetmodels", getAll)
	router.Post("/datasetmodels", create)
	router.Put("/datasetmodels/{id}", update)
	router.Get("/datasetmodels/{id}", get)
}

// @Id			datasetmodel-get
// @Tags		datasetmodels
// @Summary		Get a dataset model
// @Description	Get a single dataset model
// @Produce		json
// @Param		id	path		string	true	"Datamodel id"
// @Success		200	{object}		datasetmodel.DatasetModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/datasetmodels/{id} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	id, downloadYaml := strings.CutSuffix(id, ".yaml")

	datasetModel, err := service.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, fmt.Sprintf("Data model '%s' not found", id), err, http.StatusNotFound)
		return
	}
	if downloadYaml {
		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.yaml", id))
	}
	renderJsonOrYaml(r, w, downloadYaml, datasetModel)
}

func renderJsonOrYaml(r *http.Request, w http.ResponseWriter, downloadYaml bool, object any) {
	var response []byte
	var contentType string
	var err error

	// Convert the DatasetModel to requested format and send it as a response
	acceptHeader := r.Header.Get("Accept")
	if downloadYaml || acceptHeader == "application/yaml" || acceptHeader == "text/yaml" {
		response, err = yaml.Marshal(object)
		contentType = "application/yaml"
	} else {
		response, err = json.Marshal(object)
		contentType = "application/json"
	}

	if err != nil {
		restapi.RenderError(w, r, "Error processing dataset model", err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// @Id			datasetmodels-all
// @Tags		datasetmodels
// @Summary		Get list of dataset models
// @Description	Get list of dataset models
// @Produce		json
// @Success		200	{array}		datasetmodel.DatasetModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/datasetmodels [get]
func getAll(w http.ResponseWriter, r *http.Request) {
	detailled := r.URL.Query().Get("detailled") == "true"

	datasetModels, err := service.GetAll(detailled)
	if err != nil {
		restapi.RenderError(w, r, "Cannot fetch Data models", err)
		return
	}

	renderJsonOrYaml(r, w, false, datasetModels)
}

type datasetmodelCreated struct {
	Id string `json:"id"`
}

// @Id			datasetmodel-create
// @Summary		Create a dataset model
// @Description	Add a dataset model to the list
// @Produce		json
// @Success		200	{object}	datasetmodel.DatasetModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/datasetmodels [post]
func create(w http.ResponseWriter, r *http.Request) {
	data, ok := unmarshalDatamodel(r, w)
	if !ok {
		return
	}

	// Process the received data here
	_, err := service.Create(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not create dataset model", err)
		return
	}

	w.WriteHeader(http.StatusCreated)

	o := datasetmodelCreated{Id: data.Id}
	log.Printf("created: %v", o)
	render.DefaultResponder(w, r, &o)
}

// @Id			datasetmodel-update
// @Tags		datasetmodels
// @Summary		Update a dataset model
// @Description	Update an existing datasetmodel
// @Produce		json
// @Param		id	path		string	true	"Datamodel id"
// @Success		200	{object}		datasetmodel.DatasetModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/datasetmodels/{id} [post]
func update(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	log.Printf("update id %v", id)
	data, ok := unmarshalDatamodel(r, w)
	if !ok {
		return
	}

	if data.Id != id {
		restapi.RenderError(w, r, "Cannot change InternalId", errors.New("expected id "+id+" but document contains id "+data.Id))
		return
	}

	// Process the received data here
	err := service.Update(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not update dataset model", err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func unmarshalDatamodel(r *http.Request, w http.ResponseWriter) (datasetmodel.DatasetModel, bool) {
	contentType := r.Header.Get("Content-Type")
	log.Printf("content-type: %v", contentType)
	if contentType != "application/json" && contentType != "text/yaml" && contentType != "application/yaml" {
		http.Error(w, "Unsupported content type", http.StatusUnsupportedMediaType)
		return datasetmodel.DatasetModel{}, false
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return datasetmodel.DatasetModel{}, false
	}

	var data datasetmodel.DatasetModel
	if contentType == "application/json" {
		err = json.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid JSON", http.StatusBadRequest)
			return datasetmodel.DatasetModel{}, false
		}
	} else {
		err = yaml.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid YAML", http.StatusBadRequest)
			return datasetmodel.DatasetModel{}, false
		}
	}
	return data, true
}
