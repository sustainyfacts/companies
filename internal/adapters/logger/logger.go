package logger

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Config struct {
	Level string `mapstructure:"level" yaml:"level"`
	Env   string `mapstructure:"env" yaml:"env"` // prod|staging|dev
}

var (
	config  Config
	logger  zerolog.Logger = log.Logger                   // Global logger
	loggers                = map[string]*zerolog.Logger{} // To be able to change in Configure() method
)

func MustConfigure(c Config, version string) {
	config = c

	level := zerolog.InfoLevel
	switch c.Level {
	case zerolog.LevelDebugValue:
		level = zerolog.DebugLevel
	case zerolog.LevelInfoValue:
		level = zerolog.InfoLevel
	case zerolog.LevelWarnValue:
		level = zerolog.WarnLevel
	case zerolog.LevelErrorValue:
		level = zerolog.ErrorLevel
	}
	zerolog.SetGlobalLevel(level)

	// Needs to be after setting config var
	for pkg, l := range loggers {
		configure(l, pkg)
	}

}

// Usage
//
//	var (
//		log = logger.Get("package")
//	)
func Get(pkg string) *zerolog.Logger {
	if l, ok := loggers[pkg]; ok {
		return l
	}
	l := logger.With().Str("package", pkg).Logger()
	loggers[pkg] = &l
	return &l
}

// Replace loggers after config has been loaded
func configure(l *zerolog.Logger, pkg string) {
	*l = logger.With().Str("package", pkg).Logger()
	if config.Env == "dev" {
		*l = l.Output(zerolog.ConsoleWriter{Out: os.Stderr}) // Pretty print
	}
}
