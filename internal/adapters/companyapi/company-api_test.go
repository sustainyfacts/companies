/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package companyapi

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"go.sustainyfacts.org/companies/api/internal/domain/company"
)

const (
	baseURL = "https://api.companies.dev.sustainyfacts.com"
)

// Mock for CompanyService interface
type mocker struct {
	mock.Mock
	company.Service
}

// Mock CompanyService interface
func (mock *mocker) Search(query string, limit int) ([]company.Company, error) {
	r := mock.Called(query, limit)

	var err error
	if r[1] != nil {
		err = r[1].(error)
	}

	return r[0].([]company.Company), err
}

// Mock CompanyService interface
func (mock *mocker) GetById(id string) (company.Company, error) {
	r := mock.Called(id)

	var err error
	if r[1] != nil {
		err = r[1].(error)
	}

	return r[0].(company.Company), err
}

// Mock CompanyService interface
func (mock *mocker) Update(ctx context.Context, company company.Company) error {
	r := mock.Called(company)
	if r[0] == nil {
		return nil
	}
	return r[0].(error)
}

func setupCompanyServiceMock() *mocker {
	// Setup Mock and expected behavior
	m := &mocker{}

	// A bit funky, but maybe ok for testing
	companyService = m
	return m
}

func TestSearch(t *testing.T) {
	m := setupCompanyServiceMock()
	// Array of companies
	r := []company.Company{{Id: "123", Name: "Sustainyfacts ApS", Country: "DK"}}
	m.On("Search", "sustain", mock.AnythingOfType("int")).Return(r, nil)

	req := httptest.NewRequest("GET", baseURL+"/companies?query=sustain", nil)
	w := httptest.NewRecorder()

	// The call we want to test
	search(w, req)

	resp := w.Result()
	body, _ := io.ReadAll(resp.Body)

	expected := `[{"id":"123","name":"Sustainyfacts ApS","country":"DK"}]`

	if output := strings.TrimSpace(string(body)); output != expected {
		t.Errorf("Search should return '%v' but returned '%v'", expected, output)
	}
	// assert that the expectations were met
	m.AssertExpectations(t)
}

func TestUpdate(t *testing.T) {
	// Test data - Company
	c := company.Company{Id: "fakeid", Name: "Sustainyfacts ApS", Country: "DK", NationalID: "1234"}

	// Setup mock
	m := setupCompanyServiceMock()
	m.On("GetById", mock.Anything).Return(c, nil)
	m.On("Update", mock.Anything).Return(nil)

	// We override the getURLParam function from chi
	getURLParam = func(r *http.Request, key string) string { return "fakeid" }

	// Request that we want to test
	reqBody := bytes.NewReader([]byte(`{"Name":"Updated ApS"}`))
	req := httptest.NewRequest("PATCH", baseURL+"/companies/fakeid", reqBody)
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()

	// --- The call we want to test ---
	patch(w, req)

	resp := w.Result()
	body, _ := io.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Patch should return http status code 200 but returned %v", resp.StatusCode)
	}
	expected := `{"id":"fakeid","name":"Updated ApS","country":"DK","nationalid":"1234"}`
	if output := strings.TrimSpace(string(body)); output != expected {
		t.Errorf("Patch should return '%v' but returned '%v'", expected, output)
	}
	// assert that the expectations were met
	m.AssertExpectations(t)
}
