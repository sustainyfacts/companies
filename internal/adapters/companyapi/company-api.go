/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package companyapi

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"

	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/company"
)

// This service can be used concurrently by multiple go-routines
var companyService company.Service

func Configure(s company.Service, router chi.Router) {
	companyService = s
	// Setup router and handers

	router.Post("/companies", create)
	router.Get("/companies", search)
	router.Get("/companies/{companyref}", get)
	router.Patch("/companies/{companyref}", patch)
}

// A little trick to be able to unit test
var getURLParam = chi.URLParam

type Company struct {
	Id          string   `json:"id"`
	Name        string   `json:"name,omitempty"`
	Description string   `json:"description,omitempty"`
	Country     string   `json:"country,omitempty"`
	NationalID  string   `json:"nationalid,omitempty"`
	Website     string   `json:"website,omitempty"`
	Brands      []string `json:"brands,omitempty"`
}

// @Id			company-search
// @Tags		companies
// @Summary		Search company
// @Description	Search a company by name or brand
// @Produce		json
// @Param       query	query	string  true  "name search by query"
// @Param       limit	query	int  true  "max number of results. 20 by default"
// @Success		200	{array}		Company
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/companies [get]
func search(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	limit := r.URL.Query().Get("limit")
	intLimit, err := strconv.Atoi(limit)
	if err != nil {
		intLimit = 20
	}
	errMsg := "Search error"
	if intLimit > 200 {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("limit over 200 is not allowed"), http.StatusForbidden)
		return
	}

	// Search in lower-case only
	query = strings.ToLower(query)

	c, err := companyService.Search(query, intLimit)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}

	o := make([]Company, 0, len(c))
	for _, v := range c {
		o = append(o, transformCompany(v))
	}
	render.DefaultResponder(w, r, &o)
}

// @Id			company-get
// @Tags		companies
// @Summary		Get company
// @Description	get company by reference: Country-NationalId or Internal ID
// @Produce		json
// @Param		companyref	path		string	true	"Company Ref (Country-NationalID) or Internal Id"
// @Success		200	{object}	Company
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/companies/{companyref} [get]
func get(w http.ResponseWriter, r *http.Request) {
	ref := chi.URLParam(r, "companyref")

	c, err := getCompanyByRef(ref)

	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Company not found", err, http.StatusNotFound)
	}
	o := transformCompany(c)
	render.DefaultResponder(w, r, &o)
}

func getCompanyByRef(ref string) (company.Company, error) {
	if !strings.Contains(ref, "-") {
		id := ref
		return companyService.GetById(id)
	}
	// If the ref is Country-NationalId
	refs := strings.Split(ref, "-")
	if len(refs) != 2 {
		return company.Company{}, errors.New("ref format is Country-NationalId or Id")
	}
	return companyService.GetByNationalId(refs[0], refs[1])
}

func transformCompany(c company.Company) Company {
	return Company{Id: string(c.Id), Name: c.Name,
		Description: c.Description, Country: c.Country,
		NationalID: c.NationalID, Website: c.Website,
		Brands: c.Brands}
}

// @Id			company-create
// @Tags		companies
// @Summary		Create company
// @Description	Create a new company
// @Produce		json
// @Param		company	body		Company	true	"Company Data"
// @Success		201	{object}	Company
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/companies [post]
func create(w http.ResponseWriter, r *http.Request) {
	data := &Company{}

	errMsg := "Error creating company"
	if err := render.DefaultDecoder(r, data); err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusBadRequest)
		return
	}

	if data.Id != "" {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("field id is not allowed for creation"), http.StatusBadRequest)
		return
	}

	c := company.Company{Name: data.Name, Description: data.Description,
		Country: data.Country, NationalID: data.NationalID,
		Website: data.Website, Brands: data.Brands}

	id, err := companyService.Create(r.Context(), c)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}

	c, verr := companyService.GetById(id)
	if verr != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}
	o := transformCompany(c)

	render.Status(r, http.StatusCreated)
	render.DefaultResponder(w, r, &o)
}

// @Id			company-update
// @Tags		companies
// @Summary		Update a company
// @Description	Create a new company
// @Accept		json
// @Param		company	body		Company	true	"Company data to update"
// @Param		companyref		path		string	true	"Company Ref (Country-NationalID) or Internal Id"
// @Success		200		{object}	Company
// @Failure		400,404		{object}	restapi.ErrResponse
// @Router		/companies/{companyref} [patch]
func patch(w http.ResponseWriter, r *http.Request) {
	ref := getURLParam(r, "companyref")

	data := &Company{}

	errMsg := "Error updating company"
	if err := render.DefaultDecoder(r, data); err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusBadRequest)
		return
	}

	if data.Id != "" {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("field id is not allowed for update"), http.StatusBadRequest)
		return
	}

	c, err := getCompanyByRef(ref)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusNotFound)
		return
	}
	// Patch
	if data.Name != "" {
		c.Name = data.Name
	}
	if data.Description != "" {
		c.Description = data.Description
	}
	if data.Country != "" {
		c.Country = data.Country
	}
	if data.NationalID != "" {
		c.NationalID = data.NationalID
	}
	if data.Website != "" {
		c.Website = data.Website
	}
	if data.Brands != nil {
		c.Brands = data.Brands
	}

	err = companyService.Update(r.Context(), c)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}
	o := transformCompany(c)
	render.DefaultResponder(w, r, &o)
}
