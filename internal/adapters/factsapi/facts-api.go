/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package factsapi

import (
	"net/http"

	"go.sustainyfacts.org/companies/api/internal/adapters/logger"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
)

// This service can be used concurrently by multiple go-routines
var service facts.Service
var log = logger.Get("facts")

func Configure(s facts.Service, router chi.Router) {
	service = s
	router.Get("/facts", getFactsForCompany)
	router.Get("/facts/{id}", get)
}

// @Id			facts-get-by-company
// @Tags		facts
// @Summary		Get facts for a company
// @Description	Get facts for a company
// @Produce		json
// @Param		id	query		string	true	"Company id"
// @Success		200	{object}		facts.Facts
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/facts [get]
func getFactsForCompany(w http.ResponseWriter, r *http.Request) {
	companyId := r.URL.Query().Get("companyid")
	log.Printf("getFactsForCompany: %v", companyId)
	facts, err := service.GetByCompany(companyId)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Company '"+companyId+"' not found", err, http.StatusNotFound)
		return
	}
	render.DefaultResponder(w, r, &facts)
}

// @Id			facts-get-by-id
// @Tags		facts
// @Summary		Get facts by id
// @Description	Get facts by id
// @Produce		json
// @Param		id	path		string	true	"Facts id"
// @Success		200	{object}		datamodel.DataModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/facts/{id} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	facts, err := service.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Facts '"+id+"' not found", err, http.StatusNotFound)
		return
	}
	render.DefaultResponder(w, r, &facts)
}
