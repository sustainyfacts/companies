/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datamodelapi

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gopkg.in/yaml.v3"

	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/datamodel"
)

// This service can be used concurrently by multiple go-routines
var service datamodel.Service

var log = logger.Get("datamodel")

func Configure(s datamodel.Service, router chi.Router) {
	service = s
	router.Get("/datamodels", getAll)
	router.Post("/datamodels", create)
	router.Put("/datamodels/{id}", update)
	router.Get("/datamodels/{id}", get)
}

// @Id			datamodel-get
// @Tags		datamodels
// @Summary		Get a datamodel
// @Description	Get a single data model
// @Produce		json
// @Param		id	path		string	true	"Datamodel id"
// @Success		200	{object}		datamodel.DataModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/datamodels/{id} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	id, downloadYaml := strings.CutSuffix(id, ".yaml")

	dataModel, err := service.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Data model '"+id+"' not found", err, http.StatusNotFound)
		return
	}
	if downloadYaml {
		w.Header().Set("Content-Disposition", "attachment; filename="+dataModel.PublicId+".yaml")
	}
	renderJsonOrYaml(r, w, downloadYaml, dataModel)
}

func renderJsonOrYaml(r *http.Request, w http.ResponseWriter, downloadYaml bool, object any) {
	var response []byte
	var contentType string
	var err error

	// Convert the DataModel to requested format and send it as a response
	acceptHeader := r.Header.Get("Accept")
	if downloadYaml || acceptHeader == "application/yaml" || acceptHeader == "text/yaml" {
		response, err = yaml.Marshal(object)
		contentType = "application/yaml"
	} else {
		response, err = json.Marshal(object)
		contentType = "application/json"
	}

	if err != nil {
		restapi.RenderError(w, r, "Error processing data model", err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// @Id			datamodels-all
// @Tags		datamodels
// @Summary		Get list of datamodels
// @Description	Get list of data models
// @Produce		json
// @Success		200	{array}		datamodel.DataModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/datamodels [get]
func getAll(w http.ResponseWriter, r *http.Request) {
	detailled := r.URL.Query().Get("detailled") == "true"

	dataModels, err := service.GetAll(detailled)
	if err != nil {
		restapi.RenderError(w, r, "Cannot fetch Data models", err)
		return
	}

	renderJsonOrYaml(r, w, false, dataModels)
}

type datamodelCreated struct {
	Id string `json:"id"`
}

// @Id			datamodel-create
// @Tags		datamodels
// @Summary		Create a datamodel
// @Description	Add a data model to the list
// @Produce		json
// @Success		200	{object}		datamodel.DataModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/datamodels [post]
func create(w http.ResponseWriter, r *http.Request) {
	data, ok := unmarshalDatamodel(r, w)
	if !ok {
		return
	}

	// Process the received data here
	_, err := service.Create(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not create data model", err)
		return
	}

	w.WriteHeader(http.StatusCreated)

	o := datamodelCreated{Id: data.Id}
	log.Printf("created: %v", o)
	render.DefaultResponder(w, r, &o)
}

// @Id			datamodel-update
// @Tags		datamodels
// @Summary		Update a datamodel
// @Description	Update an existing datamodel
// @Produce		json
// @Param		id	path		string	true	"Datamodel id"
// @Success		200	{object}		datamodel.DataModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/datamodels/{id} [post]
func update(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	log.Printf("update id %v", id)
	data, ok := unmarshalDatamodel(r, w)
	if !ok {
		return
	}

	if data.Id != id {
		restapi.RenderError(w, r, "Cannot change InternalId", errors.New("expected id "+id+" but document contains id "+data.Id))
		return
	}

	// Process the received data here
	err := service.Update(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not update data model", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func unmarshalDatamodel(r *http.Request, w http.ResponseWriter) (datamodel.DataModel, bool) {
	contentType := r.Header.Get("Content-Type")
	log.Printf("content-type: %v", contentType)
	if contentType != "application/json" && contentType != "text/yaml" && contentType != "application/yaml" {
		http.Error(w, "Unsupported content type", http.StatusUnsupportedMediaType)
		return datamodel.DataModel{}, false
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return datamodel.DataModel{}, false
	}

	var data datamodel.DataModel
	if contentType == "application/json" {
		err = json.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid JSON", http.StatusBadRequest)
			return datamodel.DataModel{}, false
		}
	} else {
		err = yaml.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid YAML", http.StatusBadRequest)
			return datamodel.DataModel{}, false
		}
	}
	return data, true
}
