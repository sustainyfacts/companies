/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"github.com/segmentio/ksuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

const (
	field_original_id = "original_id"
	field_version     = "version"
	_history          = "_history"
	no_limit          = 0
)

type Collection[E Keyed] struct {
	name               string
	searchKeywordField string
	db                 *mongo.Database
	generateID         func() string
}

type Keyed interface {
	Key() string
}

type Versioned[E any] interface {
	Keyed
	NewVersion(newId string) E
}

type VersionedCollection[E Versioned[E]] struct {
	metadata string
	Collection[E]
}

func NewCollection[E Keyed](db *mongo.Database, generateID func() string, name string) *Collection[E] {
	c := &Collection[E]{name: name, db: db, generateID: generateID}
	return c
}

func NewVersionedCollection[E Versioned[E]](db *mongo.Database, generateID func() string, name string, metadata string) *VersionedCollection[E] {
	c := Collection[E]{name: name, db: db, generateID: generateID}
	return &VersionedCollection[E]{Collection: c, metadata: metadata}
}

func (c *Collection[E]) GenerateID() string {
	return c.generateID()
}

func (c *Collection[E]) MustEnsureSearchIndex(ctx context.Context, searchKeywordField string, searchFields ...string) {
	c.searchKeywordField = searchKeywordField

	keys := make([]primitive.E, len(searchFields))
	for i, v := range searchFields {
		keys[i] = primitive.E{Key: v, Value: "text"}
	}

	searchIndex := mongo.IndexModel{Keys: keys, Options: options.Index().SetName(searchIndexName)}

	keywordIndex := mongo.IndexModel{Keys: bson.D{{Key: c.searchKeywordField, Value: 1}},
		Options: options.Index().SetName(keywordIndexName)}

	indexes := c.Get().Indexes()
	go c.createOrReplaceIndex(ctx, indexes, searchIndex)
	go c.createOrReplaceIndex(ctx, indexes, keywordIndex)
}

func (c *Collection[E]) MustEnsureUniqueIndex(ctx context.Context, indexFields ...string) {
	keys := make([]primitive.E, len(indexFields))
	for i, v := range indexFields {
		keys[i] = primitive.E{Key: v, Value: 1}
	}

	uniqueIndex := mongo.IndexModel{Keys: keys, Options: options.Index().SetUnique(true)}

	indexes := c.Get().Indexes()
	go c.createOrReplaceIndex(ctx, indexes, uniqueIndex)
}

func (c *Collection[E]) MustEnsureIndex(ctx context.Context, indexFields ...string) {
	keys := make([]primitive.E, len(indexFields))
	for i, v := range indexFields {
		keys[i] = primitive.E{Key: v, Value: 1}
	}

	uniqueIndex := mongo.IndexModel{Keys: keys}

	indexes := c.Get().Indexes()
	go c.createOrReplaceIndex(ctx, indexes, uniqueIndex)
}

func (c *Collection[E]) Get() *mongo.Collection {
	return c.db.Collection(c.name)
}

func (c *Collection[E]) Save(entity E) error {
	companies := c.Get()

	opts := options.Replace().SetUpsert(true)
	filter := bson.D{{Key: "_id", Value: entity.Key()}}

	_, err := companies.ReplaceOne(context.TODO(), filter, &entity, opts)

	if err != nil {
		log.Printf("Error saving entity %v", err)
	}

	return err
}

func (c *VersionedCollection[E]) MustEnsureHistory(ctx context.Context) {
	c.MustEnsureIndex(ctx, c.metadata+"."+field_original_id, c.metadata+"."+field_version)
}

func (c *VersionedCollection[E]) getHistory() *mongo.Collection {
	return c.db.Collection(c.name + _history)
}

func (c *VersionedCollection[E]) Save(entity E) error {
	// Save the entity
	err := c.Collection.Save(entity)
	if err != nil {
		return err
	}

	// We generate an id for saving a copy for versioning
	historyId, _ := ksuid.NewRandom()
	v := entity.NewVersion(historyId.String())

	// Save the version history
	_, err = c.getHistory().InsertOne(context.Background(), &v)

	if err != nil {
		log.Printf("Error saving entity history %v", err)
	}
	return err
}

func (c *VersionedCollection[E]) FindVersions(id string) ([]history.Version, error) {
	matchStage := bson.D{{Key: "$match", Value: bson.M{"metadata.original_id": id}}}
	replaceRootStage := bson.D{{Key: "$replaceRoot", Value: bson.M{"newRoot": "$metadata"}}}

	cursor, err := c.getHistory().Aggregate(context.Background(), mongo.Pipeline{matchStage, replaceRootStage})

	return decodeManyObjects[history.Version](cursor, err, max_disclosures)
}

func (c *VersionedCollection[E]) LoadVersion(id string, version int) (E, error) {
	collection := c.getHistory()
	var entity E

	filter := bson.D{{Key: c.metadata + "." + field_original_id, Value: id}, {Key: c.metadata + "." + field_version, Value: version}}

	err := collection.FindOne(context.TODO(), filter).Decode(&entity)

	return entity, err
}

func (c *Collection[E]) Load(id string) (E, error) {
	return c.LoadUniqueAttribute("_id", id)
}

func (c *Collection[E]) LoadUniqueAttribute(attribute string, id string) (E, error) {
	collection := c.Get()
	var entity E
	filter := bson.D{{Key: attribute, Value: id}}

	err := collection.FindOne(context.TODO(), filter).Decode(&entity)

	return entity, err
}

func (c *Collection[E]) LoadAll() ([]E, error) {
	collection := c.Get()

	cursor, err := collection.Find(context.TODO(), bson.M{})

	return c.decodeMany(cursor, err, no_limit)
}

func (c *Collection[E]) Search(query string, limit int) ([]E, error) {
	collection := c.Get()

	byText := bson.D{{Key: "$text", Value: bson.D{{Key: "$search", Value: query}}}}
	regex := "^" + query
	byKeyword := bson.D{{Key: c.searchKeywordField, Value: bson.D{{Key: "$regex", Value: regex}}}}

	filter := bson.D{{Key: "$or", Value: []bson.D{byText, byKeyword}}}

	cursor, err := collection.Find(context.TODO(), filter)

	return c.decodeMany(cursor, err, limit)
}

func (c *Collection[E]) decodeMany(cursor *mongo.Cursor, err error, limit int) ([]E, error) {
	return decodeManyObjects[E](cursor, err, limit)
}

func decodeManyObjects[E any](cursor *mongo.Cursor, err error, limit int) ([]E, error) {
	defer cursor.Close(context.TODO())

	if err != nil {
		return nil, err
	}

	var results = make([]E, 0)
	for cursor.Next(context.TODO()) {
		var result E
		if err := cursor.Decode(&result); err != nil {
			return nil, err
		}
		results = append(results, result)

		if limit != no_limit && len(results) >= limit {
			// We have enough results
			break
		}
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

const (
	searchIndexName  = "searchIndex"
	keywordIndexName = "keywordIndex"
)

func (c *Collection[E]) createOrReplaceIndex(ctx context.Context, indexes mongo.IndexView, searchIndex mongo.IndexModel) {
	_, err := indexes.CreateOne(ctx, searchIndex)
	if err != nil {
		name := searchIndex.Options.Name
		log.Printf("Replacing index %v", name)
		_, err = indexes.DropOne(ctx, *name)
		if err != nil {
			log.Fatal().Msgf("Could not drop index: %v", err)
		}
		_, err = indexes.CreateOne(ctx, searchIndex)
	}
	if err != nil {
		log.Fatal().Msgf("Could not create index: %v", err)
	}
}
