/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.sustainyfacts.org/companies/api/internal/domain/rating"
)

type ratingRepository struct {
	*VersionedCollection[rating.CompanyRating]
}

func NewRatingRepository(ctx context.Context, db *mongo.Database, generateID func() string) ratingRepository {
	disclosures := NewVersionedCollection[rating.CompanyRating](db, generateID, "ratings", "metadata")
	disclosures.MustEnsureHistory(ctx)
	disclosures.MustEnsureIndex(ctx, "company_id")
	return ratingRepository{disclosures}
}

// Implements disclosure.Repository interface
func (repo *ratingRepository) FindByCompany(companyId string) (rating.CompanyRating, error) {
	return repo.LoadUniqueAttribute("company_id", companyId)
}
