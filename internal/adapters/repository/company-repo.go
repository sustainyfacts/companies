/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.sustainyfacts.org/companies/api/internal/domain/company"
)

type companiesRepository struct {
	*Collection[company.Company]
}

func NewCompaniesRepository(ctx context.Context, db *mongo.Database, generateID func() string) companiesRepository {
	companies := NewCollection[company.Company](db, generateID, "companies")
	companies.MustEnsureSearchIndex(ctx, "metadata.keywords", "name", "description", "brands")
	companies.MustEnsureUniqueIndex(ctx, "country", "nationalid")
	return companiesRepository{companies}
}

// Implement company.Repository interface
func (repo *companiesRepository) FindByCountryAndNationalId(country string, nationalId string) (company.Company, error) {
	filter := bson.D{{Key: "country", Value: country}, {Key: "nationalid", Value: nationalId}}

	company := company.Company{}
	err := repo.Get().FindOne(context.TODO(), filter).Decode(&company)

	return company, err
}
