/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type factsRepository struct {
	// We wrap the collection to be able to fix the bson unmarshalling
	fc *VersionedCollection[facts.Facts]
}

func NewFactsRepository(ctx context.Context, db *mongo.Database, generateID func() string) factsRepository {
	facts := NewVersionedCollection[facts.Facts](db, generateID, "facts", "metadata")
	facts.MustEnsureHistory(ctx)
	facts.MustEnsureUniqueIndex(ctx, "company_id")
	return factsRepository{facts}
}

// Implements facts.Repository interface
func (repo *factsRepository) FindByCompany(companyId string) (facts.Facts, error) {
	return fixUnmarshal(repo.fc.LoadUniqueAttribute("company_id", companyId))
}

// Implements facts.Repository interface
func (repo *factsRepository) Load(id string) (facts.Facts, error) {
	return fixUnmarshal(repo.fc.Load(id))
}

// Implements facts.Repository interface
func (repo *factsRepository) LoadVersion(id string, version int) (facts.Facts, error) {
	return fixUnmarshal(repo.fc.LoadVersion(id, version))
}

// Implements facts.Repository interface
func (repo *factsRepository) FindVersions(id string) ([]history.Version, error) {
	return repo.fc.FindVersions(id)
}

// Implements facts.Repository interface
func (repo *factsRepository) GenerateID() string {
	return repo.fc.generateID()
}

// Implements facts.Repository interface
func (repo *factsRepository) Save(facts facts.Facts) error {
	return repo.fc.Save(facts)
}

func fixUnmarshal(f facts.Facts, err error) (facts.Facts, error) {
	if err != nil {
		return f, err
	}
	f.FixbsonUnmarshal()
	return f, nil
}
