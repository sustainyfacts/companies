/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
)

const (
	max_disclosures = 1000
	max_versions    = 1000
)

type disclosuresRepository struct {
	*VersionedCollection[disclosure.Disclosure]
}

func NewDisclosuresRepository(ctx context.Context, db *mongo.Database, generateID func() string) disclosuresRepository {
	disclosures := NewVersionedCollection[disclosure.Disclosure](db, generateID, "disclosures", "metadata")
	disclosures.MustEnsureHistory(ctx)
	disclosures.MustEnsureIndex(ctx, "company_id")
	return disclosuresRepository{disclosures}
}

// Implements disclosure.Repository interface
func (repo *disclosuresRepository) FindByCompany(companyId string) ([]disclosure.Disclosure, error) {
	filter := bson.D{{Key: "company_id", Value: companyId}}

	cursor, err := repo.Get().Find(context.TODO(), filter)

	return repo.decodeMany(cursor, err, max_disclosures)
}

// Implements disclosure.Repository interface
func (repo *disclosuresRepository) FindPublishedByCompany(companyId string) ([]disclosure.Disclosure, error) {
	filter := bson.D{{Key: "company_id", Value: companyId},
		{Key: "is_published", Value: true}}

	cursor, err := repo.Get().Find(context.TODO(), filter)

	return repo.decodeMany(cursor, err, max_disclosures)
}
