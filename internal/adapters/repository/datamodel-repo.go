/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.sustainyfacts.org/companies/api/internal/domain/datamodel"
)

type datamodelsRepository struct {
	*VersionedCollection[datamodel.DataModel]
}

func NewDataModelRepository(ctx context.Context, db *mongo.Database, generateID func() string) datamodelsRepository {
	dataModels := NewVersionedCollection[datamodel.DataModel](db, generateID, "datamodels", "metadata")
	dataModels.MustEnsureHistory(ctx)
	return datamodelsRepository{dataModels}
}

// Implement datamodel.Repository interface. Does not load disclosures
func (repo *datamodelsRepository) FindAll() ([]datamodel.DataModel, error) {
	collection := repo.Get()

	// We do not return disclosures
	findOptions := options.Find().SetProjection(bson.M{
		"disclosures": 0,
	})
	cursor, err := collection.Find(context.TODO(), bson.M{}, findOptions)

	return decodeManyObjects[datamodel.DataModel](cursor, err, no_limit)
}
