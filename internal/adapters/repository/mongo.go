/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"
	"io"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
)

var (
	mongoClient *mongo.Client
	log         = logger.Get("repository")
)

// The CloserFunc type is an adapter to allow the use of
// ordinary functions as Closer
type CloserFunc func() error

// Close calls f().
func (f CloserFunc) Close() error {
	return f()
}

// Connect to Mongo
func MustConnectMongo(ctx context.Context, mongoURI string) io.Closer {
	if mongoClient != nil {
		panic("Mongo already initialized")
	}

	log.Info().Msg("Connecting to mongoDB")
	connectionCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	mc, err := mongo.Connect(connectionCtx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatal().Msgf("Aborting: Unable to connect to Mongo: %v", err)
	}
	if err := mc.Ping(connectionCtx, readpref.Primary()); err != nil {
		log.Fatal().Msgf("Aborting: Unable to ping Mongo: %v", err)
	}
	mongoClient = mc
	log.Info().Msg("Successfully connected to Mongo")

	return CloserFunc(func() error {
		err := mongoClient.Disconnect(ctx)
		if err != nil {
			log.Printf("Unable to disconnect from Mongo: %v", err)
		}
		return err
	})
}

func GetDatabase(database string) *mongo.Database {
	db := mongoClient.Database(database)
	log.Printf("Connected to Mongo database: %v", db.Name())
	return db
}
