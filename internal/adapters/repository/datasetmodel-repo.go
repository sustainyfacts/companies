/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package repository

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.sustainyfacts.org/companies/api/internal/domain/datasetmodel"
)

type datasetmodelsRepository struct {
	*VersionedCollection[datasetmodel.DatasetModel]
}

func NewDatasetModelRepository(ctx context.Context, db *mongo.Database, generateID func() string) datasetmodelsRepository {
	datasetModels := NewVersionedCollection[datasetmodel.DatasetModel](db, generateID, "datasetmodels", "metadata")
	datasetModels.MustEnsureHistory(ctx)
	return datasetmodelsRepository{datasetModels}
}

// Implement datamodel.Repository interface. Does not load mappings
func (repo *datasetmodelsRepository) FindAll() ([]datasetmodel.DatasetModel, error) {
	collection := repo.Get()

	// We do not return mappings
	findOptions := options.Find().SetProjection(bson.M{
		"mappings": 0,
	})
	cursor, err := collection.Find(context.TODO(), bson.M{}, findOptions)

	return decodeManyObjects[datasetmodel.DatasetModel](cursor, err, no_limit)
}
