/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package broker

import (
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
)

var log = logger.Get("broker")

type FakeBroker struct {
	subscribers map[string][]func(message any)
}

func NewFakeBroker() *FakeBroker {
	subscribers := make(map[string][]func(any), 0)
	return &FakeBroker{subscribers: subscribers}
}
func (b *FakeBroker) Publish(topic string, message any) {
	log.Printf("Received message '%v' on topic %v", topic, message)

	if handlers, exists := b.subscribers[topic]; exists {
		for _, handler := range handlers {
			// Handle in a go-routine
			go handler(message)
		}
	}
}

func (b *FakeBroker) Subscribe(topic string, handler func(message any)) {
	_, ok := b.subscribers[topic]
	if !ok {
		b.subscribers[topic] = make([]func(any), 0)
	}

	b.subscribers[topic] = append(b.subscribers[topic], handler)
	log.Printf("Added subscriber to topic %v", topic)
}
