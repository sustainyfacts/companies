/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package utils

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/segmentio/ksuid"
)

const (
	sfIdRegexString = `^([a-z]{1}[a-z0-9_]{0,20})(\.[a-z0-9]{1}[a-z0-_]{0,20})*$`
)

var (
	sfIdRegex = regexp.MustCompile(sfIdRegexString)
	// validator.Validate should be used as a singleton
	validate *validator.Validate
)

func init() {
	validate = validator.New()
	validate.RegisterValidation("has_values", hasValue)
	validate.RegisterValidation("sfid", isSfId)
}

// Something like ghg.scope_2.market
func isSfId(fl validator.FieldLevel) bool {
	return sfIdRegex.MatchString(fl.Field().String())
}

// Checks that the map contains values matching ALL the expressions given as parameter.
// Expressions:
//   - are separated by ;
//   - if they end with * they are matched as prefix
//
// Usage:
//
//	Mappings map[string]string `validate:"required,has_values=prefix*;value"``
func hasValue(fl validator.FieldLevel) bool {
	fieldValue := fl.Field()
	if fieldValue.Kind() != reflect.Map {
		return false
	}
	params := strings.Split(fl.Param(), ";")

	for _, param := range params {
		param, isPrefixSearch := strings.CutSuffix(param, "*")

		for _, key := range fieldValue.MapKeys() {
			v := fieldValue.MapIndex(key).String()
			if isPrefixSearch && strings.HasPrefix(v, param) || v == param {
				return true
			}
		}
	}

	return false
}

// Validate is a function that validates a struct using validate: labels on its fields
// It "Implements" company.Validate
func Validate(s any) error {
	return validate.Struct(s)
}

// GenerateID is a function that generates a random ID with ksuid
func GenerateID() string {
	id, _ := ksuid.NewRandom()
	return id.String()
}
