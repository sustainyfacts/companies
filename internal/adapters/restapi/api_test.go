/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package restapi

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

const (
	baseURL = "https://api.companies.dev.sustainyfacts.com"
)

type mockResponseWriter struct {
	// To avoid having to mock all methods from the interface
	// we embed the interface inside the struct
	http.ResponseWriter

	// To capture the data being written
	sb strings.Builder
}

// Implementation of interface http.ResponseWriter
func (mock *mockResponseWriter) Write(b []byte) (int, error) {
	return mock.sb.Write(b)
}

// This is a home-made mock
func TestHelloWithMock(t *testing.T) {
	mock := &mockResponseWriter{sb: strings.Builder{}}

	// The call we want to test
	hello(mock, &http.Request{})

	if output := mock.sb.String(); output != "Sustainyfacts Company API" {
		t.Errorf("Hello should have written 'Sustainyfacts Company API' but wrote instead %v", output)
	}
}

// Test with standard httptest objects
func TestHelloWithHttptest(t *testing.T) {
	req := httptest.NewRequest("GET", baseURL, nil)
	w := httptest.NewRecorder()

	// The call we want to test
	hello(w, req)

	resp := w.Result()
	body, _ := io.ReadAll(resp.Body)

	if output := string(body); output != "Sustainyfacts Company API" {
		t.Errorf("Hello should have written 'Sustainyfacts Company API' but wrote instead %v", output)
	}
}
