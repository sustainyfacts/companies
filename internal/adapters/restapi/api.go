/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package restapi

import (
	"context"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.sustainyfacts.org/companies/api/docs"
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"

	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

var (
	router              *chi.Mux
	allowedContentTypes = []string{"application/json", "application/yaml", "text/yaml"}
	jwkCachedSet        jwk.Set
	log                 = logger.Get("api")
)

// General configuration of the service
func Configure(apiURL string, uiURL string, jwtCertsURL string, version string, noAuth bool) chi.Router {

	log.Printf("Starting API at %v", apiURL)
	log.Printf("Accepting UI calls from %v", uiURL)
	log.Printf("Using Certs Store %v", jwtCertsURL)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// The first steps are the same as examples/jwk_cache_example_test.go
	c := jwk.NewCache(ctx)
	c.Register(jwtCertsURL, jwk.WithMinRefreshInterval(1*time.Hour))
	_, err := c.Refresh(ctx, jwtCertsURL)
	if err != nil {
		log.Fatal().Msgf("failed to load sustainyfacts JWKS: %s\n", err)
	}
	jwkCachedSet = jwk.NewCachedSet(c, jwtCertsURL)

	api, _ := url.Parse(apiURL)

	if router != nil {
		panic("Router already initialized")
	}
	router = chi.NewRouter()

	router.Use(middleware.Heartbeat("/ping")) // Simple ping service for checking availability
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)    // Must come before anything that changes response
	router.Use(middleware.Recoverer) // Handles and recovers from panics
	router.Use(middleware.AllowContentType(allowedContentTypes...))
	router.Use(middleware.ContentCharset("", "UTF-8"))
	router.Use(middleware.Timeout(1 * time.Minute))
	router.Use(middleware.Compress(6)) // Should be between flate.BestSpeed and flate.BestCompression; flate.DefaultCompression is same as 6

	setupCorsMiddleware(router, uiURL)

	// Authenticated functions
	authRouter := router.Group(func(r chi.Router) {
		// Seek, verify and validate JWT tokens
		// Needs to be after CORS as OPTION requests do not include Authorization headers
		if !noAuth {
			r.Use(VerifyToken(jwtCertsURL))
		} else {
			r.Use(AnonymousUser)
			log.Printf("<=== WARNING: Authentication disabled ===>")
		}
		r.Post("/login", login(api.Host, jwtCertsURL))
		r.Get("/logout", logout(api.Host))
	})

	docs.SwaggerInfo.Title = "Sustainyfacts Data API"
	docs.SwaggerInfo.Description = "This API allows searching, editing and creation of Companies and related sustainability data"
	docs.SwaggerInfo.Version = version
	docs.SwaggerInfo.Host = api.Host
	docs.SwaggerInfo.BasePath = "/"
	docs.SwaggerInfo.Schemes = []string{api.Scheme}

	router.Get("/docs/*", httpSwagger.Handler(
		httpSwagger.URL(api.Scheme+"://"+api.Host+"/docs/doc.json"), //The url pointing to API definition
	))

	router.Get("/", hello)

	return authRouter
}

type tokens struct {
	AccessToken string `json:"access_token"`
	IdToken     string `json:"id_token"`
}

func logout(cookieDomain string) http.HandlerFunc {
	hfn := func(w http.ResponseWriter, r *http.Request) {
		cookie := http.Cookie{
			Name:     "jwt",
			Value:    "",
			HttpOnly: true,
			Secure:   true,
			Domain:   cookieDomain,
			SameSite: http.SameSiteLaxMode,
			// Removing a cookie is done by expiring it
			Expires: time.Now().Add(-time.Minute),
		}
		http.SetCookie(w, &cookie)
	}
	return hfn
}
func login(cookieDomain string, jwtCertsURI string) http.HandlerFunc {
	hfn := func(w http.ResponseWriter, r *http.Request) {
		tokenData := tokens{}
		if err := render.DefaultDecoder(r, &tokenData); err != nil {
			log.Fatal().Msgf("Error: %v", err)
		}
		idToken, err := jwt.Parse([]byte(tokenData.IdToken), getTokenParseOptions(jwtCertsURI))
		if err != nil {
			log.Print(err)
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}
		if t := tokenData.AccessToken; t != "" {
			// No expiration, which means this is a session cookie
			cookie := http.Cookie{
				Name:     "jwt",
				Value:    t,
				HttpOnly: true,
				Secure:   true,
				Domain:   "." + cookieDomain,
				SameSite: http.SameSiteLaxMode,
			}
			http.SetCookie(w, &cookie)

		}

		name := getStringFromToken(idToken, "name")
		email := getStringFromToken(idToken, "email")
		sub := idToken.Subject()
		// TODO(RNO) parse token create user in mongo based on info in ID token and return name and email
		render.DefaultResponder(w, r, map[string]string{"name": name, "email": email, "sub": sub})
	}
	return hfn
}

func getStringFromToken(idToken jwt.Token, s string) string {
	ns, ok := idToken.Get(s)
	if !ok {
		return ""
	}
	return ns.(string)
}

// Provides dummy identity when auth is disabled
func AnonymousUser(next http.Handler) http.Handler {
	hfn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		// Add User Id to context
		ctx = context.WithValue(ctx, intf.UserIdCtxKey, "anonymous")
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(hfn)
}

func VerifyToken(jwtCertsURI string) func(http.Handler) http.Handler {
	h := func(next http.Handler) http.Handler {
		hfn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			tokenString := getTokenFromRequest(r)
			token, err := jwt.Parse([]byte(tokenString), getTokenParseOptions(jwtCertsURI))
			if err != nil {
				log.Print(err)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			err = jwt.Validate(token)

			// Add User Id to context
			ctx = context.WithValue(ctx, intf.UserIdCtxKey, token.Subject())

			if err != nil {
				log.Print(err)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r.WithContext(ctx))
		}
		return http.HandlerFunc(hfn)
	}
	return h
}

// getTokenFromHeader tries first to retrieve the token string from the
// "Authorization" request header ("Authorization: BEARER T"),
// or from a cookie named "jwt"
func getTokenFromRequest(r *http.Request) string {
	// Get token from authorization header first.
	if bearer := r.Header.Get("Authorization"); bearer != "" {
		if len(bearer) > 7 && strings.ToUpper(bearer[0:6]) == "BEARER" {
			return bearer[7:]
		}
	}
	// Fallback to jwt cookie
	cookie, err := r.Cookie("jwt")
	if err != nil {
		// No cookie found
		return ""
	}
	return cookie.Value
}

func getTokenParseOptions(jwtCertsURI string) jwt.ParseOption {
	return jwt.WithKeySet(jwkCachedSet)
}

func hello(w http.ResponseWriter, request *http.Request) {
	w.Write([]byte("Sustainyfacts Company API"))
}

func Start() {
	if router == nil {
		panic("Service not configured")
	}

	// And then start the http listener
	log.Info().Msg("API Service listening on port 80")
	err := http.ListenAndServe(":http", router)
	if err != nil {
		log.Error().Err(err).Msg("Error - HTTP service stopped")
	}
}

// Sets up CORS Middleware
func setupCorsMiddleware(router *chi.Mux, uiURI string) {
	allowedOrigin := uiURI
	router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{allowedOrigin},
		AllowedMethods:   []string{"GET", "PATCH", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "Origin", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
		MaxAge:           30, // Maximum value not ignored by any of major browsers
	}))
}

func RenderError(w http.ResponseWriter, r *http.Request, msg string, err error) {
	status := http.StatusInternalServerError
	if _, ok := err.(validator.ValidationErrors); ok {
		status = http.StatusUnprocessableEntity
	}
	RenderErrorWithStatus(w, r, msg, err, status)
}

func RenderErrorWithStatus(w http.ResponseWriter, r *http.Request, msg string, err error, httpStatusCode int) {
	render.Status(r, httpStatusCode)
	render.DefaultResponder(w, r, ErrResponse{
		StatusText: msg,
		ErrorText:  err.Error(),
	})
}

// ErrResponse renderer type for handling all sorts of errors.
// Can we use httputil.HTTPError instead
type ErrResponse struct {
	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}
