/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package ratingmodelapi

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gopkg.in/yaml.v3"

	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
)

// This service can be used concurrently by multiple go-routines
var service ratingmodel.Service

var log = logger.Get("ratingmodel")

func Configure(s ratingmodel.Service, router chi.Router) {
	service = s
	router.Get("/ratingmodels", getAll)
	router.Post("/ratingmodels", create)
	router.Put("/ratingmodels/{id}", update)
	router.Get("/ratingmodels/{id}", get)
}

// @Id			ratingmodel-get
// @Tags		ratingmodels
// @Summary		Get a ratingmodel
// @Description	Get a single rating model
// @Produce		json
// @Param		id	path		string	true	"Ratingmodel id"
// @Success		200	{object}		ratingmodel.RatingModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/ratingmodels/{id} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	id, downloadYaml := strings.CutSuffix(id, ".yaml")

	model, err := service.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Rating model '"+id+"' not found", err, http.StatusNotFound)
		return
	}
	if downloadYaml {
		w.Header().Set("Content-Disposition", "attachment; filename="+model.PublicId+".yaml")
	}
	renderJsonOrYaml(r, w, downloadYaml, model)
}

func renderJsonOrYaml(r *http.Request, w http.ResponseWriter, downloadYaml bool, object any) {
	var response []byte
	var contentType string
	var err error

	// Convert the RatingModel to requested format and send it as a response
	acceptHeader := r.Header.Get("Accept")
	if downloadYaml || acceptHeader == "application/yaml" || acceptHeader == "text/yaml" {
		response, err = yaml.Marshal(object)
		contentType = "application/yaml"
	} else {
		response, err = json.Marshal(object)
		contentType = "application/json"
	}

	if err != nil {
		restapi.RenderError(w, r, "Error processing data model", err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// @Id			ratingmodels-all
// @Tags		ratingmodels
// @Summary		Get list of ratingmodels
// @Description	Get list of rating models
// @Produce		json
// @Success		200	{array}		ratingmodel.RatingModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/ratingmodels [get]
func getAll(w http.ResponseWriter, r *http.Request) {
	ratingModels, err := service.GetAll(false)
	if err != nil {
		restapi.RenderError(w, r, "Cannot fetch Data models", err)
		return
	}

	renderJsonOrYaml(r, w, false, ratingModels)
}

type ratingmodelCreated struct {
	Id string `json:"public_id"`
}

// @Id			ratingmodel-create
// @Tags		ratingmodels
// @Summary		Create a ratingmodel
// @Description	Add a rating model to the list
// @Produce		json
// @Success		200	{object}		ratingmodel.RatingModel
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/ratingmodels [post]
func create(w http.ResponseWriter, r *http.Request) {
	data, ok := unmarshalRatingModel(r, w)
	if !ok {
		return
	}

	// Process the received data here
	_, err := service.Create(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not create data model", err)
		return
	}

	w.WriteHeader(http.StatusCreated)

	o := ratingmodelCreated{Id: data.Id}
	log.Printf("created: %v", o)
	render.DefaultResponder(w, r, &o)
}

// @Id			ratingmodel-update
// @Tags		ratingmodels
// @Summary		Update a ratingmodel
// @Description	Update an existing ratingmodel
// @Produce		json
// @Param		id	path		string	true	"Ratingmodel id"
// @Success		200	{object}		ratingmodel.RatingModel
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/ratingmodels/{id} [post]
func update(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	log.Printf("update id %v", id)
	data, ok := unmarshalRatingModel(r, w)
	if !ok {
		return
	}

	if data.Id != id {
		restapi.RenderError(w, r, "Cannot change Id", errors.New("expected id "+id+" but document contains id "+data.Id))
		return
	}

	// Process the received data here
	err := service.Update(r.Context(), data)
	if err != nil {
		restapi.RenderError(w, r, "Could not update rating model", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func unmarshalRatingModel(r *http.Request, w http.ResponseWriter) (ratingmodel.RatingModel, bool) {
	contentType := r.Header.Get("Content-Type")
	log.Printf("content-type: %v", contentType)
	if contentType != "application/json" && contentType != "text/yaml" && contentType != "application/yaml" {
		http.Error(w, "Unsupported content type", http.StatusUnsupportedMediaType)
		return ratingmodel.RatingModel{}, false
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return ratingmodel.RatingModel{}, false
	}

	var data ratingmodel.RatingModel
	if contentType == "application/json" {
		err = json.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid JSON", http.StatusBadRequest)
			return ratingmodel.RatingModel{}, false
		}
	} else {
		err = yaml.Unmarshal(body, &data)
		if err != nil {
			http.Error(w, "Invalid YAML", http.StatusBadRequest)
			return ratingmodel.RatingModel{}, false
		}
	}
	return data, true
}
