/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package disclosureapi

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"

	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
)

// This service can be used concurrently by multiple go-routines
var disclosureService disclosure.Service

func Configure(s disclosure.Service, router chi.Router) {
	disclosureService = s
	// Setup router and handers

	router.Post("/disclosures", create)
	router.Get("/disclosures", getByCompany)
	router.Get("/disclosures/{disclosureid}", get)
	router.Patch("/disclosures/{disclosureid}", patch)
	router.Get("/disclosures/{disclosureid}/versions", getVersions)
	router.Get("/disclosures/{disclosureid}/versions/{version}", getByVersion)
}

// A little trick to be able to unit test
var getURLParam = chi.URLParam

type Disclosure struct {
	Id          string                    `json:"id"`
	CompanyId   string                    `json:"company_id,omitempty"`
	Type        string                    `json:"type,omitempty"`
	IsPublished *bool                     `json:"is_published,omitempty"`
	Year        int                       `json:"year,omitempty"`
	Currency    string                    `json:"currency,omitempty"`
	Url         string                    `json:"url,omitempty"`
	Standards   []string                  `json:"standards,omitempty"`
	Data        map[string]map[string]any `json:"data,omitempty"`
}

// @Id			disclosure-get
// @Tags		disclosures
// @Summary		Get disclosure
// @Description	get disclosure by id
// @Produce		json
// @Param		disclosureid	path		string	true	"Disclosure Id"
// @Success		200	{object}		disclosure.Disclosure
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/disclosures/{disclosureid} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "disclosureid")

	disclosure, err := disclosureService.GetById(id)

	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Disclosure not found", err, http.StatusNotFound)
		return
	}
	o := transformDisclosure(disclosure)
	render.DefaultResponder(w, r, &o)
}

// @Id			disclosure-getbyversion
// @Tags		disclosures
// @Summary		Get disclosure by version
// @Description	get disclosure by id and version
// @Produce		json
// @Param		disclosureid			path		string	true	"Disclosure Id"
// @Param		version				path		integer	true	"Version"
// @Success		200	{object}		disclosure.Disclosure
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/disclosures/{disclosureid}/versions/${version} [get]
func getByVersion(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "disclosureid")
	version, err := strconv.Atoi(chi.URLParam(r, "version"))
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Invalid version", err, http.StatusBadRequest)
		return
	}

	disclosure, err := disclosureService.GetByVersion(id, version)

	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Disclosure not found", err, http.StatusNotFound)
		return
	}
	o := transformDisclosure(disclosure)
	render.DefaultResponder(w, r, &o)
}

func transformDisclosure(r disclosure.Disclosure) Disclosure {
	return Disclosure{Id: string(r.Id), CompanyId: r.CompanyId, Type: string(r.Type), IsPublished: &r.IsPublished, Year: r.Year,
		Currency: r.Currency, Url: r.Url, Standards: r.Standards, Data: r.Data}
}

// @Id			disclosure-getversions
// @Tags		disclosures
// @Summary		Get disclosure versions
// @Description	get disclosure versions by id
// @Produce		json
// @Param		disclosureid	path		string	true	"Disclosure Id"
// @Success		200	{array}			history.Version
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/disclosures/{disclosureid}/versions [get]
func getVersions(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "disclosureid")

	versions, err := disclosureService.GetVersions(id)

	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Disclosure not found", err, http.StatusNotFound)
		return
	}
	render.DefaultResponder(w, r, &versions)
}

// @Id			disclosure-getbycompany
// @Tags		disclosures
// @Summary		Get disclosures
// @Description	Get disclosures by company
// @Produce		json
// @Param       companyid	query	string  true  "Id of the company to retrieve disclosures from"
// @Success		200	{array}		disclosure.Disclosure
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/disclosures [get]
func getByCompany(w http.ResponseWriter, r *http.Request) {
	companyId := r.URL.Query().Get("companyid")
	errMsg := "Search error"

	if companyId == "" {
		restapi.RenderError(w, r, errMsg, errors.New("companyid parameter not specified"))
		return
	}
	c, err := disclosureService.GetByCompany(companyId)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	} else if c == nil {
		restapi.RenderError(w, r, errMsg, errors.New("company not found"))
		return
	}

	o := make([]Disclosure, 0, len(c))
	for _, v := range c {
		o = append(o, transformDisclosure(v))
	}
	render.DefaultResponder(w, r, &o)
}

// @Id			disclosure-create
// @Tags		disclosures
// @Summary		Create disclosure
// @Description	Create a new disclosure
// @Produce		json
// @Param		disclosure	body	disclosure.Disclosure	true	"Disclosure Data"
// @Success		201	{object}	disclosure.Disclosure
// @Failure		400	{object}	restapi.ErrResponse
// @Router		/disclosures [post]
func create(w http.ResponseWriter, r *http.Request) {
	data := &Disclosure{}

	errMsg := "Error creating disclosure"
	if err := render.DefaultDecoder(r, data); err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusBadRequest)
		return
	}

	if data.Id != "" {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("field id is not allowed for creation"), http.StatusBadRequest)
		return
	}

	isPublished := data.IsPublished != nil && *data.IsPublished
	c := disclosure.Disclosure{Year: data.Year, CompanyId: data.CompanyId, Type: disclosure.Type(data.Type), IsPublished: isPublished,
		Currency: data.Currency, Url: data.Url, Standards: data.Standards, Data: data.Data}

	id, err := disclosureService.Create(r.Context(), c)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}

	c, verr := disclosureService.GetById(id)
	if verr != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}
	o := transformDisclosure(c)

	render.Status(r, http.StatusCreated)
	render.DefaultResponder(w, r, &o)
}

// @Id			update
// @Tags		disclosures
// @Summary		Update a disclosure
// @Description	Create a new disclosure
// @Accept		json
// @Param		disclosure		body		disclosure.Disclosure	true	"Disclosure data to update"
// @Param		disclosureid	path		string			true	"Disclosure Id"
// @Success		200		{object}		disclosure.Disclosure
// @Failure		400,404		{object}	restapi.ErrResponse
// @Router		/disclosures/{disclosureid} [patch]
func patch(w http.ResponseWriter, r *http.Request) {
	id := getURLParam(r, "disclosureid")

	data := &Disclosure{}

	errMsg := "Error updating disclosure"
	if err := render.DefaultDecoder(r, data); err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusBadRequest)
		return
	}

	if data.Id != "" {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("field id is not allowed for update"), http.StatusBadRequest)
		return
	} else if data.CompanyId != "" {
		restapi.RenderErrorWithStatus(w, r, errMsg, errors.New("field company is not allowed for update"), http.StatusBadRequest)
		return
	}

	c, err := disclosureService.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, errMsg, err, http.StatusNotFound)
		return
	}
	// Patch
	if data.IsPublished != nil {
		c.IsPublished = *data.IsPublished
	}
	if data.Type != "" {
		c.Type = disclosure.Type(data.Type)
	}
	if data.Year != 0 {
		c.Year = data.Year
	}
	if data.Currency != "" {
		c.Currency = data.Currency
	}
	if data.Standards != nil {
		c.Standards = data.Standards
	}
	if data.Url != "" {
		c.Url = data.Url
	}
	if data.Data != nil {
		c.Data = data.Data
	}

	err = disclosureService.Update(r.Context(), c)
	if err != nil {
		restapi.RenderError(w, r, errMsg, err)
		return
	}
	o := transformDisclosure(c)
	render.DefaultResponder(w, r, &o)
}
