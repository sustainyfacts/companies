/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package evaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParamsWithDash(t *testing.T) {
	params := map[string]any{
		`constant-with-dash`: 1,
		"constant2":          2,
		"constant3":          3,
	}
	result := Evaluate(`constant-with-dash + constant2 + constant3`, params)

	if result != 6 {
		t.Errorf("Expressions value should be 6 but was %#v", result)
	}
}

func TestParamsWithUnderscore(t *testing.T) {
	params := map[string]any{
		"constant":      1,
		"constant_2022": 2,
		"constant_2023": 3,
	}
	result := Evaluate("constant + constant_2022 + constant_2023", params)

	if result != 6 {
		t.Errorf("Expressions value should be 6 but was %#v", result)
	}
}

func TestParamsWithArray(t *testing.T) {
	array := [...]int{1, 2, 3, 4, 5, 6}
	params := map[string]any{
		"constant": 1,
		"index":    4,
		"array":    array,
		"slice":    array[:],
	}

	testcases := []testCase{
		{"constant + sum(array)", 22},
		{"constant + array[0] + array[1] + slice[2]", 7},
		{"constant + array[2]**array[1]", 10},
		{"constant + slice[2]**slice[1]", 10},
		{"constant + sum(slice[:3])", 7},
		{"constant + sum(slice[index-2:index])", 8},
		{"slice[index]", 5},
	}
	for _, tc := range testcases {
		evaluateAndTestWithParam(tc, t, params)
	}
}

func TestParamsWithYears(t *testing.T) {
	year := 2023
	years := make([]int, year+1)
	metric1 := make([]int, year+1)
	metric2 := make([]int, year+1)
	for i := 1; i <= year; i++ {
		years[i] = i
		metric1[i] = 10000 + i
		metric2[i] = 20000 + i
	}

	params := map[string]any{
		"years":        years,
		"current_year": year,
		"metric1":      metric1,
		"metric2":      metric2,
	}

	testcases := []testCase{
		{"years[2023]", 2023},
		{"years[current_year]", 2023},
		{"sum(years[:300])", 300 * (300 - 1) / 2},
		{"metric1[2023]", 12023},
		{"metric2[current_year]", 22023},
		{"sum(metric1[-1:])", 12023},
		{"sum(metric1[-2:])", 24045},
		{"count(metric2, # < 20100)", 100},
	}
	for _, tc := range testcases {
		evaluateAndTestWithParam(tc, t, params)
	}
}

func TestPredicates(t *testing.T) {
	values := []any{nil, 1, 2, 3}
	params := map[string]any{
		"my_values": values,
	}

	testcases := []testCase{
		{"all(my_values[-3:], # != nil)", true},
		{"all(my_values, # != nil)", false},
		{"count(my_values, # != nil)", 3},
	}
	for _, tc := range testcases {
		t.Run("Evaluate "+tc.in, func(t *testing.T) {
			evaluateAndTestWithParam(tc, t, params)
		})
	}
}

type simpleStruct struct{ V1, V2 int }

func TestParamsWithStruct(t *testing.T) {
	params := map[string]any{
		"constant": 1,
		"object": struct {
			Att1, Att2, Att3 int
			Att4             simpleStruct
		}{Att1: 1, Att2: 2, Att3: 3, Att4: simpleStruct{V1: 4, V2: 5}},
	}
	testcases := []testCase{
		{"constant + object.Att1 + object.Att2 + object.Att3", 7},
		{"constant + object.Att4.V1", 5},
	}
	for _, tc := range testcases {
		t.Run("Evaluate "+tc.in, func(t *testing.T) {
			evaluateAndTestWithParam(tc, t, params)
		})
	}
}

func TestParamsWithMap(t *testing.T) {
	params := map[string]any{
		"constant": 1,
		"object": map[string]any{
			"att1":   1,
			"att-2":  2,
			"att_3":  3,
			"att4":   simpleStruct{V1: 4, V2: 5},
			"att5":   map[string]int{"v3": 6, "v4": 7},
			"att.v6": 8,
		},
	}

	testcases := []testCase{
		{"constant + object['att1']", 2},
		{"constant + object.att1", 2},
		{"constant + object['att-2']", 3},
		{"constant + object.att_3", 4},
		{"constant + object.att4.V1", 5},
		{"constant + object.att5.v3 + object['att5'].v4", 14},
		{"constant + object['att.v6']", 9}, // object.att.v6 not supported
	}
	for _, tc := range testcases {
		t.Run("Evaluate "+tc.in, func(t *testing.T) {
			evaluateAndTestWithParam(tc, t, params)
		})
	}
}

type testCase struct {
	in   string
	want any
}

func TestIntConversion(t *testing.T) {
	testcases := []testCase{
		{"'Hello, world'", "Hello, world"},
		{"' '", " "},
		{"1", 1},
		{"1.0", 1},
		{"1.0 + 3", 4},
		{"1.1 + 3", 4.1},
		{"0.0001*10000", 1},
		{"(1/1000000)*1000000.0", 1},
		{"981724.0000001", 981724.0000001},
		{"981724.000000", 981724},
		// We do some rounding to support the case below
		{"1000.0000000001", 1000},
		// Following works before we do some rounding as float arimethics is not perfect
		{"0.1 + 0.2", 0.3},
		{"0.1 + 0.2 + 0.1 + 0.2 + 0.1 + 0.2 + 0.1", 1},
		// Testing the limits of the rounding
		{"1000.000000001", 1000.000000001},
		{"123456789000.00001", 123456789000.00001},
	}
	for _, tc := range testcases {
		t.Run("Evaluate "+tc.in, func(t *testing.T) {
			evaluateAndTest(tc, t)
		})
	}
}

func evaluateAndTestWithParam(tc testCase, t *testing.T, params map[string]any) {
	result := Evaluate(tc.in, params)
	assert.Equalf(t, tc.want, result, `Evaluate(%v)`, tc.in)
}

func evaluateAndTest(tc testCase, t *testing.T) {
	evaluateAndTestWithParam(tc, t, nil)
}

func TestFunctions(t *testing.T) {
	testcases := []testCase{
		{"lavg(1,2,3,4,5)", 3},
		{"lsum(1,2,3,4,5)", 15},
		{"lsum([1,2,3],[4,5])", 15},
		{"lsum(1,2,3,4,5) / 5", 3},
	}
	for _, tc := range testcases {
		t.Run("Evaluate "+tc.in, func(t *testing.T) {
			evaluateAndTest(tc, t)
		})
	}
}

func TestToSlice(t *testing.T) {
	array := [6]int{1, 2, 3, 4, 5, 6}
	slice, ok := toSlice(array)
	if !ok {
		t.Errorf("Result: %v\n", slice)
	}

	slice, ok = toSlice(array[:3])
	if !ok {
		t.Errorf("Result: %v\n", slice)
	}
}
