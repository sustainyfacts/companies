/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package evaluator

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"strings"

	"go.sustainyfacts.org/companies/api/internal/adapters/logger"

	"github.com/expr-lang/expr"
)

const (
	isDebugEnabled    = false
	maxFloatPrecision = 9
)

var log = logger.Get("main")

func Evaluate(expression string, parameters map[string]any) any {
	result := evaluateWithExpr(expression, parameters)
	debug("Evaluate %v returns %v", expression, result)
	return result
}

func evaluateWithExpr(expression string, parameters map[string]any) any {
	env := make(map[string]any, len(parameters))
	// Escape parameters: replace - with _
	for k := range parameters {
		if strings.ContainsAny(k, "-") && strings.Contains(expression, k) {
			paramWith_ := strings.ReplaceAll(k, "-", "_")
			expression = strings.ReplaceAll(expression, k, paramWith_)
			env[paramWith_] = parameters[k]
		} else {
			env[k] = parameters[k]
		}
	}

	// Add the standard functions
	env["lavg"] = lavg
	env["lsum"] = lsum

	result, err := expr.Eval(expression, env)
	if err != nil {
		log.Printf("Error: %v", err)
		return nil
	}

	return maybeInt(result)
}

// Sum of a list of arguments
func lsum(args ...any) (any, error) {
	debug("fsum(%v)", args)

	var sum float64 = 0
	for index, arg := range args {
		if f, ok := arg.(float64); ok {
			sum += f
		} else if i, ok := arg.(int); ok {
			sum += float64(i)
		} else if a, ok := toSlice(arg); ok {
			// We sum sub-arrays
			s, err := lsum(a...)
			if err != nil {
				return nil, err
			}
			sum += s.(float64)
		} else {
			return nil, fmt.Errorf("argument with index %v is not a number: %v", index, arg)
		}
	}
	return sum, nil
}

func debug(msg string, args ...any) {
	if isDebugEnabled {
		log.Printf(msg, args...)
	}
}

func toSlice(v any) ([]any, bool) {
	rv := reflect.ValueOf(v)
	kind := rv.Kind()

	if kind == reflect.Slice || kind == reflect.Array {
		length := rv.Len()
		slice := make([]any, length)
		for i := 0; i < length; i++ {
			slice[i] = rv.Index(i).Interface()
		}
		return slice, true
	}
	return nil, false
}

func lavg(args ...any) (any, error) {
	count := len(args)
	if count == 0 {
		return nil, errors.New("cannot divide by zero")
	}
	sum, err := lsum(args...)
	if err != nil {
		return nil, err
	}
	return sum.(float64) / float64(count), nil
}

func maybeInt(value any) any {
	// Check if the float64 value is an integer
	if floatValue, ok := value.(float64); ok {
		floatValue = roundFloat(floatValue, maxFloatPrecision)
		if float64(int(floatValue)) == floatValue {
			// If it is an integer, return an int
			return int(floatValue)
		}
		return floatValue
	}

	// Otherwise, return the original value
	return value
}

func roundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}
