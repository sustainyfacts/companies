/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package ratingapi

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/domain/rating"
)

// This service can be used concurrently by multiple go-routines
var service rating.Service

func Configure(s rating.Service, router chi.Router) {
	service = s
	router.Get("/ratings", getRatingsForCompany)
	router.Get("/ratings/{id}", get)
}

// @Id			ratings-get-by-company
// @Tags		rating
// @Summary		Get ratings for a company
// @Description	Get ratings for a company
// @Produce		json
// @Param		id	query		string	true	"Company id"
// @Success		200	{object}		rating.CompanyRating
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/ratings [get]
func getRatingsForCompany(w http.ResponseWriter, r *http.Request) {
	companyId := r.URL.Query().Get("companyid")
	ratings, err := service.GetByCompany(companyId)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Company '"+companyId+"' not found", err, http.StatusNotFound)
		return
	}
	render.DefaultResponder(w, r, &ratings)
}

// @Id			rating-get-by-id
// @Tags		rating
// @Summary		Get rating by id
// @Description	Get rating by id
// @Produce		json
// @Param		id	path		string	true	"Ratings id"
// @Success		200	{object}		rating.CompanyRating
// @Failure		400,404	{object}	restapi.ErrResponse
// @Router		/ratings/{id} [get]
func get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	facts, err := service.GetById(id)
	if err != nil {
		restapi.RenderErrorWithStatus(w, r, "Rating '"+id+"' not found", err, http.StatusNotFound)
		return
	}
	render.DefaultResponder(w, r, &facts)
}
