package facts

import (
	"embed"
	"encoding/json"
	"io/fs"
	"testing"

	"github.com/go-test/deep"
	"go.mongodb.org/mongo-driver/bson"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
)

func readObjectFromFile[E any](filename string) (E, error) {
	var object E
	bytes, err := fs.ReadFile(files, filename)
	if err != nil {
		return object, err
	}

	err = json.Unmarshal(bytes, &object)
	return object, err
}

//go:embed test-data/*
var files embed.FS

func TestAggregateDisclosures(t *testing.T) {
	disclosures, err := readObjectFromFile[[]disclosure.Disclosure]("test-data/disclosures_test.json")
	if err != nil {
		t.Errorf("Error reading disclosures_test.json: %v", err)
	}

	facts, err := readObjectFromFile[Facts]("test-data/facts_test.json")
	if err != nil {
		t.Errorf("Error reading facts_test.json: %v", err)
	}

	aggregatedData, err := aggregateDisclosures(disclosures)
	if err != nil {
		t.Errorf("Error aggregating disclosures: %v", err)
	}

	expectedData := facts.Data

	if aggregatedData.CompanyId != facts.CompanyId {
		t.Errorf("Incorrect CompanyId: got %s, want %s", aggregatedData.CompanyId, facts.CompanyId)
	}

	if aggregatedData.Currency != facts.Currency {
		t.Errorf("Incorrect Currency: got %s, want %s", aggregatedData.Currency, facts.Currency)
	}

	if aggregatedData.FromYear != facts.FromYear {
		t.Errorf("Incorrect FromYear: got %d, want %d", aggregatedData.FromYear, facts.FromYear)
	}

	if aggregatedData.ToYear != facts.ToYear {
		t.Errorf("Incorrect ToYear: got %d, want %d", aggregatedData.ToYear, facts.ToYear)
	}

	log.Printf("Expected: %+v\n", expectedData)
	log.Printf("Aggregated: %+v\n", aggregatedData.Data)
	diff := deep.Equal(aggregatedData.Data, expectedData)
	if diff != nil {
		t.Errorf("Data not aggregated correctly: %v", diff)
	}
}

func TestBsonMarshalling(t *testing.T) {
	facts := Facts{Id: "fakeid", CompanyId: "2NjsJgKydeWkP34oIASqaMSGTP2",
		FromYear: 2018, ToYear: 2022, Currency: "DKK",
		Data: map[string]map[string]any{
			"sf": {
				"tst_001": []any{65, nil, 1, nil, 7},
				"tst_002": []any{nil, nil, []any{2.5}, []any{1.1, 2.2, 3.3}, []any{1.11, 2.22, 3.33}},
			},
			"ghg": {
				"tst_005": map[string][]any{
					"energy":     {nil, nil, 5, 50, 50},
					"percentage": {nil, nil, 6, 50, 25},
				},
				"tst_006": []any{nil, nil,
					[]any{map[string]any{"target_value": 40, "target_year": 2040},
						map[string]any{"target_value": 0, "target_year": 2050}},
					[]any{map[string]any{"target_value": 77, "target_year": 122}}, nil}},
		},
	}
	originaljsonbytes, _ := json.Marshal(facts)

	// We marshal to bson
	bsonFacts, _ := bson.Marshal(&facts)
	resultFacts := Facts{}
	// And then unmarshal
	bson.Unmarshal(bsonFacts, &resultFacts)

	// Fix the bson unmarshalling
	resultFacts.FixbsonUnmarshal()

	// Then we compare the json output of the object
	// Because bson unmarshals with primitives A,D,E which means the objects are not equal
	jsonbytes, _ := json.Marshal(resultFacts)

	got := string(jsonbytes)
	expected := string(originaljsonbytes)

	if got != expected {
		t.Errorf("Facts not marshalled+unmarshalled properly. Expected %v, but got %v", expected, got)
	}
}
