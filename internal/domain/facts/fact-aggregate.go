package facts

import (
	"errors"
	"sort"

	"go.mongodb.org/mongo-driver/bson"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type Facts struct {
	Id        string `bson:"_id" validate:"required" json:"id"`                // auto-generated uid
	CompanyId string `bson:"company_id" validate:"required" json:"company_id"` // uid of the Company
	FromYear  int    `bson:"from_year" validate:"required" json:"from_year"`   // First year of data
	ToYear    int    `bson:"to_year" validate:"required" json:"to_year"`       // Last year of data
	Currency  string `validate:"required,iso4217" json:"currency"`             // The currency used in the disclosure

	// Aggregated Data and Metrics
	// ---------------------------
	// This is a map with the data model and metric ids as keys, and the values are arrays.
	// The first item of the array is the data for the first year in the dataset (FromYear),
	// and each subsequent item contains the data for the next year until the last year (ToYear)
	Data map[string]map[string]any `validate:"omitempty" json:"data"`

	// Metadata: timestamps, changed by
	// Should not be exported/imported in json/yaml
	Metadata history.Version `validate:"omitempty" json:"-"`
}

// Implement Keyed interface
func (f Facts) Key() string {
	return f.Id
}

// Implement Versioned interface
func (f Facts) NewVersion(newid string) Facts {
	// Ensure original Id is correct
	f.Metadata.OriginalId = f.Id
	return Facts{Id: newid, CompanyId: f.CompanyId, FromYear: f.FromYear, ToYear: f.ToYear,
		Currency: f.Currency, Data: f.Data, Metadata: f.Metadata}
}

// This method replaces bson.D with bson.M as the default unmarshaller
// chooses bson.D for elements inside an array, which is not what we want
func (f *Facts) FixbsonUnmarshal() {
	for _, dm := range f.Data {
		for k, v := range dm {
			dm[k] = fixbson(v)
		}
	}
}

func fixbson(o any) any {
	switch b := o.(type) {
	case bson.A:
		for i := range b {
			b[i] = fixbson(b[i])
		}
	case bson.D:
		m := b.Map()
		fixbson(m)
		return m
	case bson.M:
		for k := range b {
			b[k] = fixbson(b[k])
		}
	}
	return o
}

func aggregateDisclosures(disclosures []disclosure.Disclosure) (Facts, error) {
	if len(disclosures) == 0 {
		return Facts{}, errors.New("no disclosures provided")
	}

	sort.Slice(disclosures, func(i, j int) bool {
		return disclosures[i].Year < disclosures[j].Year
	})

	companyId := disclosures[0].CompanyId
	currency := disclosures[0].Currency

	for _, disclosure := range disclosures {
		if disclosure.CompanyId != companyId {
			return Facts{}, errors.New("inconsistent company IDs")
		}
		if disclosure.Currency != currency {
			return Facts{}, errors.New("inconsistent currencies")
		}
	}
	fromYear := disclosures[0].Year
	toYear := disclosures[len(disclosures)-1].Year
	numberOfYears := toYear - fromYear + 1

	aggregatedData := Facts{
		Id:        disclosures[0].Id,
		CompanyId: companyId,
		FromYear:  fromYear,
		ToYear:    toYear,
		Currency:  currency,
		Data:      make(map[string]map[string]any),
	}

	for _, disclosure := range disclosures {
		for modelId, disclosureData := range disclosure.Data {
			factsData, ok := aggregatedData.Data[modelId]
			if !ok {
				factsData = make(map[string]any)
				aggregatedData.Data[modelId] = factsData
			}
			for metricId := range disclosureData {
				factsMetric, ok := factsData[metricId]
				if !ok {
					factsMetric = createYearArrays(disclosureData[metricId], numberOfYears)
					factsData[metricId] = factsMetric
				}
				copyMetricData(disclosureData[metricId], factsMetric, disclosure.Year-fromYear)
			}
		}
	}

	return aggregatedData, nil
}

// This method creates the fact data structure as needed:
//   - data points (integers, booleans, floats, strings) into arrays of data (one item per year)
//   - Maps are kept are replicated (processed recursively for underlying data points)
//   - Arrays are transformed into an array of array (first array is one item per year)
func createYearArrays(disclosureMetric any, numberOfYears int) any {
	log.Printf("convertToMultipleYears - %+v, numberOfYears: %v", disclosureMetric, numberOfYears)
	switch d := disclosureMetric.(type) {
	case map[string]any:
		result := make(map[string]any, numberOfYears)
		for k, v := range d {
			result[k] = createYearArrays(v, numberOfYears)
		}
		return result
	default:
		return make([]any, numberOfYears)
	}
}

// This method update the fact data based on input diclosure data:
//   - data points (integers, booleans, floats, strings) are copied to the correct year entry (index)
//   - Maps are processed recursively for underlying data points
//   - Arrays are copied to the correct year entry (index)
func copyMetricData(disclosureMetric any, targetMetric any, index int) {
	switch d := disclosureMetric.(type) {
	case map[string]any:
		for k, v := range d {
			copyMetricData(v, targetMetric.(map[string]any)[k], index)
		}
	default:
		targetMetric.([]any)[index] = d
	}
	log.Printf("copyMetricData - disclosure: %+v, target: %+v, index: %v", disclosureMetric, targetMetric, index)
}
