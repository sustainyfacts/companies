/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package facts

import (
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

var log = logger.Get("facts")

type Service interface {
	UpdateCompanyFacts(companyId string, disclosure ...disclosure.Disclosure) (Facts, error)
	GetByCompany(companyId string) (Facts, error)
	GetById(id string) (Facts, error)
}

type Repository interface {
	intf.VersionedRepository[Facts]
	FindByCompany(companyId string) (Facts, error)
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo           Repository                                              // Dependency inversion to persistence framework
	validate       Validate                                                // Dependency inversion to validate framework
	broker         intf.Broker                                             // Messsage broker
	getDisclosures func(companyId string) ([]disclosure.Disclosure, error) // Dependency inversion to load disclosures
}

// Dependency inversion to keep domain dependencies clean
type Validate func(object any) error // object validation

func NewService(repo Repository, validate Validate, broker intf.Broker, getDisclosures func(companyId string) ([]disclosure.Disclosure, error)) Service {
	service := &serviceImpl{repo: repo, validate: validate, broker: broker, getDisclosures: getDisclosures}
	broker.Subscribe(intf.Topic_CompanyDisclosureUpdated, service.handleCompanyDisclosureUpdated)
	return service
}

// Receive
func (s *serviceImpl) handleCompanyDisclosureUpdated(message any) {
	companyId, ok := message.(string)
	if !ok {
		log.Printf("Error: handleCompanyDisclosureUpdated: message should be of type string. Received %v", message)
		return
	}

	disclosures, err := s.getDisclosures(companyId)
	if err != nil {
		log.Printf("Error: aggregating disclosures for company %v", companyId)
		return
	}
	facts, err := aggregateDisclosures(disclosures)
	if err != nil {
		log.Printf("Error: aggregating disclosures for company %v", companyId)
		return
	}

	if existingFacts, err := s.repo.FindByCompany(companyId); err == nil {
		// If there is an existing document, we update it
		facts.Id = existingFacts.Id
		facts.Metadata = existingFacts.Metadata

	} else {
		facts.Id = s.repo.GenerateID()
	}
	// Update metadata
	facts.Metadata.Update("unkown-user-id")

	s.repo.Save(facts)

	// We publish an event for rating calculation
	s.broker.Publish(intf.Topic_CompanyFactsUpdated, companyId)
}

// Implements Service
func (s *serviceImpl) UpdateCompanyFacts(companyId string, disclosure ...disclosure.Disclosure) (Facts, error) {
	return Facts{}, nil
}

// Implements Service
func (s *serviceImpl) GetById(id string) (Facts, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (Facts, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId
	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetByCompany(companyId string) (Facts, error) {
	return s.repo.FindByCompany(companyId)
}
