/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package intf

import (
	"context"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

var (
	UserIdCtxKey = &contextKey{"UserId"}
)

func GetUserId(ctx context.Context) string {
	if userId, ok := ctx.Value(UserIdCtxKey).(string); ok {
		return userId
	}
	return ""
}

type CrudService[E any] interface {
	GetById(id string) (E, error)
	Create(ctx context.Context, entity E) (string, error)
	Update(ctx context.Context, entity E) error
}

type CrudVersionedService[E any] interface {
	CrudService[E]
	GetByVersion(id string, version int) (E, error)
	GetVersions(id string) ([]history.Version, error)
}

// contextKey is a value for use with context.WithValue. It's used as
// a pointer so it fits in an interface{} without allocation. This technique
// for defining context keys was copied from Go 1.7's new use of context in net/http.
type contextKey struct {
	name string
}

func (k *contextKey) String() string {
	return "jwtauth context value " + k.name
}
