/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package intf

const (
	Topic_CompanyDisclosureUpdated = "company_disclosure_updated"
	Topic_DatasetUpdated           = "dataset_updated"
	Topic_CompanyFactsUpdated      = "company_facts_updated"
)

type Publisher interface {
	Publish(topic string, message any)
}

type Subscriber interface {
	Subscribe(topic string, messageHanlder func(message any))
}

type Broker interface {
	Publisher
	Subscriber
}
