/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package history

import "time"

type Version struct {
	UpdatedTime time.Time `bson:"updated_time" validate:"required" json:"updated_time"`
	UpdatedBy   string    `bson:"updated_by" validate:"required" json:"updated_by"`
	Version     int       `validate:"required" json:"version"`
	OriginalId  string    `bson:"original_id" validate:"required" json:"original_id"`
}

func (m *Version) Update(user string) {
	// Updating metadata
	m.Version++
	m.UpdatedTime = time.Now()
	m.UpdatedBy = user
}
