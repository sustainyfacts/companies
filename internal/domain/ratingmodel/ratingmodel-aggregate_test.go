/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package ratingmodel

import (
	"embed"
	"io/fs"
	"testing"

	"github.com/go-test/deep"
	"gopkg.in/yaml.v3"
)

//go:embed test-data/*
var embedfs embed.FS

func loadTestRatingModel(t *testing.T) RatingModel {
	yamlFile, err := fs.ReadFile(embedfs, "test-data/ratingmodel-test.yaml")
	if err != nil {
		panic("error reading ratingmodel-test.yaml file")
	}
	var ratingModel RatingModel
	err = yaml.Unmarshal(yamlFile, &ratingModel)
	if err != nil {
		t.Errorf("Error parsing YAML file: %s\n", err)
	}
	return ratingModel
}

func TestYaml(t *testing.T) {
	parsed := loadTestRatingModel(t)

	declared := RatingModel{Id: "sfcli",
		PublicId:    "SF-CLIMATE",
		Name:        "Sustainyfacts Climate Rating Model",
		Description: "Sustainyfacts test rating model",
		Version:     "0.1",
		GlobalMetrics: []Metric{
			{
				MetricID:    "sf_cli_scope123",
				Description: "Total emissions of a company",
				Unit:        "metric_tons_CO2e",
				Expression:  "sf_cli_scope1 + sf_cli_scope2 + sf_cli_scope3"}},
		Grades: []Grade{
			{Grade: "A", Requirements: []Requirement{
				{Description: "Achieving net-zero greenhouse gas (GHG) emissions or being carbon negative, ahead of the 2050 target set by the Paris Agreement",
					Conditions: []string{"sf_cli_scope123 <= 0 || sf_cli_netzero-year < 2050"}},
				{Description: "Do not overshoot Paris agreement carbon budget (using 2015 as baseline)",
					Conditions: []string{
						"sf_cli_scope1 <= sf_cli_scope1[2015]*(1-(current_year-2015)/(2050-2015))",
						"sf_cli_scope123 <= sf_cli_scope123[2015]*(1-(current_year-2015)/(2050-2015))"}}}},
			{Grade: "B", Requirements: []Requirement{
				{Description: "Net-zero ambition ahead of the 2050 target set by the Paris Agreement",
					Conditions: []string{"sf_cli_netzero-year < 2050"}},
				{Description: "Slight overshoot (<10%) of Paris agreement carbon budget (using 2015 as baseline)",
					Conditions: []string{
						"(sf_cli_scope1 - sf_cli_scope1[2015]*(1-(current_year-2015)/(2050-2015))) / sf_cli_scope1[2015]*(1-(current_year-2015)/(2050-2015)) <= 0.1"}}}}}}
	diff := deep.Equal(parsed, declared)
	if diff != nil {
		t.Error(diff)
	}
}
