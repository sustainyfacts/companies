/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package ratingmodel

import (
	"context"
	"errors"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

type Service interface {
	intf.CrudVersionedService[RatingModel]
	GetAll(detailled bool) ([]RatingModel, error)
}

type Repository interface {
	intf.VersionedRepository[RatingModel]
	FindAll() ([]RatingModel, error) // Returns only overview, without grades and global_metrics
	LoadAll() ([]RatingModel, error) // Returns the full objects
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo     Repository // Provides persistence
	validate Validate   // Provides validation of structs
}

// Dependency inversion to avoid exposing validator
type Validate func(s interface{}) error

func NewService(repo Repository, validate Validate) Service {
	return &serviceImpl{repo: repo, validate: validate}
}

// Implements Service
func (s *serviceImpl) GetById(id string) (RatingModel, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (RatingModel, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId
	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetAll(detailled bool) ([]RatingModel, error) {
	if detailled {
		return s.repo.LoadAll()
	}
	return s.repo.FindAll()
}

// Implements Service
func (s *serviceImpl) Update(ctx context.Context, r RatingModel) error {
	if r.Id == "" {
		return errors.New("id must be set")
	}

	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return err
	}

	err = s.repo.Save(r)
	if err != nil {
		return err
	}

	return nil
}

// Implements Service
func (s *serviceImpl) Create(ctx context.Context, r RatingModel) (string, error) {
	if r.Id == "" {
		return "", errors.New("id must be set")
	}

	// Updating metadata
	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return "", err
	}

	err = s.repo.Save(r)
	if err != nil {
		return "", err
	}
	return r.Id, nil
}
