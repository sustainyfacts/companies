/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package ratingmodel

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type RatingModel struct {
	Id            string   `validate:"required,alphanum,lowercase" bson:"_id" yaml:"id" json:"id"`                  // Internal uid
	PublicId      string   `validate:"required" bson:"public_id" yaml:"public_id" json:"public_id"`                 // Internal uid
	Name          string   `validate:"required" yaml:"name" json:"name"`                                            // Name of the rating model
	Description   string   `bson:"description,omitempty" yaml:"description,omitempty" json:"description,omitempty"` // public version
	Version       string   `bson:"version,omitempty" yaml:"version,omitempty" json:"version,omitempty"`             // version
	GlobalMetrics []Metric `bson:"global_metrics,omitempty" json:"global_metrics,omitempty" yaml:"global_metrics,omitempty"`
	// Global metrics. If there are dependencies between them, they should be declared in the order they need to be computed
	Grades []Grade `json:"grades" yaml:"grades"`

	// Metadata: timestamps, changed by
	// Should not be exported/imported in json/yaml
	Metadata history.Version `yaml:"-" json:"-" validate:"omitempty"`
}

func (dm RatingModel) Key() string {
	return dm.Id
}

func (r RatingModel) NewVersion(newId string) RatingModel {
	// Ensure original Id is correct
	r.Metadata.OriginalId = r.Id
	return RatingModel{Id: newId, Name: r.Name, Description: r.Description, Version: r.Version, Grades: r.Grades, GlobalMetrics: r.GlobalMetrics, Metadata: r.Metadata}
}

// Global metrics used by the model or ouput of the model itself
type Metric struct {
	MetricID    string `validate:"required,sfid" bson:"metric_id" json:"metric_id" yaml:"metric_id"` // id of the metric, for referencing in the requirement conditions
	Description string `json:"description"`                                                          // Description of the metric
	Unit        string `bson:"unit,omitempty" yaml:"unit,omitempty" json:"unit,omitempty"`           // Unit of the metric, if relevant
	Expression  string `json:"expression"`                                                           // An expression that can be evaluated to a number (float64)
}

type Grade struct {
	Grade        string        `json:"grade" yaml:"grade"`               // A grade given in a rating model
	Requirements []Requirement `json:"requirements" yaml:"requirements"` // The list of requirements to obtain the grade
}

type Requirement struct {
	Description     string   `json:"description" yaml:"description"` // Description of the requirement
	InternalMetrics []Metric `bson:"internal_metrics,omitempty" json:"internal_metrics,omitempty" yaml:"internal_metrics,omitempty"`
	Conditions      []string `json:"conditions" yaml:"conditions"` // List of conditions to fullfill the requirement. A Condition is an expression that can be evaluated into a boolean
}
