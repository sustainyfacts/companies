/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type CompanyRating struct {
	Id            string                   `validate:"required" bson:"_id"  yaml:"id" json:"id"`             // Internal uid
	CompanyId     string                   `validate:"required" bson:"company_id" json:"company_id"`         // uid of the Company
	LatestRatings map[string]Rating        `bson:"latest_ratings,omitempty" json:"latest_ratings,omitempty"` // Latest Ratings, using the rating_model_id as key
	AllRatings    map[string]RatingHistory `bson:"all_ratings,omitempty" json:"all_ratings,omitempty"`       // All historical ratings

	// Metadata: timestamps, changed by
	// Should not be exported/imported in json/yaml
	Metadata history.Version `json:"-" validate:"omitempty"`
}

type Rating struct {
	Grade   any            `validate:"required" bson:"grade" json:"grade"`     // Grade given by the rating
	Year    int            `validate:"required" bson:"year" json:"year"`       // Year of the rating
	Metrics map[string]any `bson:"metrics,omitempty" json:"metrics,omitempty"` // Metrics calculated by the rating model, using the metric_id as key
}

type RatingHistory struct {
	// Fields are arrays with one item for each year:
	// The first item corresponds to "FromYear" and the the last item corresponds to "ToYear"
	FromYear int              `validate:"required" bson:"from_year" json:"from_year"` // First year with ratings
	ToYear   int              `validate:"required" bson:"to_year" json:"to_year"`     // Last year with ratings
	Grades   []any            `validate:"required" json:"grades"`                     // Grades given by the rating
	Metrics  map[string][]any `bson:"metrics,omitempty" json:"metrics,omitempty"`     // Metrics calculated by the rating model, using the metric_id as key
}

func (d CompanyRating) Key() string {
	return d.Id
}

func (d CompanyRating) NewVersion(newid string) CompanyRating {
	// Ensure original Id is correct
	d.Metadata.OriginalId = d.Id
	return CompanyRating{Id: newid, CompanyId: d.CompanyId,
		LatestRatings: d.LatestRatings,
		AllRatings:    d.AllRatings,
		Metadata:      d.Metadata}
}

func (cr *CompanyRating) getGrade(modelId string, year int) any {
	if cr.AllRatings == nil {
		log.Printf("getGrade return '' because there are no ratings")
		return nil
	}
	ratings, ok := cr.AllRatings[modelId]
	if !ok {
		log.Printf("getGrade return '' because there are no ratings for model %v", modelId)
		return nil
	}
	if year < ratings.FromYear || ratings.ToYear < year {
		log.Printf("getGrade return '' because year %v is out of range [%v-%v]", year, ratings.FromYear, ratings.ToYear)
		return nil
	}
	return ratings.Grades[year-ratings.FromYear]
}

func (cr *CompanyRating) getMetric(modelId string, metricId string, year int) any {
	if cr.AllRatings == nil {
		log.Printf("getMetric return '' because there are no ratings")
		return nil
	}
	ratings, ok := cr.AllRatings[modelId]
	if !ok {
		log.Printf("getMetric return '' because there are no ratings for model %v", modelId)
		return nil
	}
	if year < ratings.FromYear || ratings.ToYear < year {
		log.Printf("getMetric return '' because year %v is out of range [%v-%v]", year, ratings.FromYear, ratings.ToYear)
		return nil
	}
	values, ok := ratings.Metrics[metricId]
	if !ok {
		log.Printf("getMetric return '' because there are no metric %v for model %v", metricId, modelId)
		return nil
	}
	return values[year-ratings.FromYear]
}

func (cr *CompanyRating) AddRating(modelId string, rating *Rating) {
	if cr.LatestRatings == nil {
		cr.LatestRatings = make(map[string]Rating)
	}
	if cr.AllRatings == nil {
		cr.AllRatings = make(map[string]RatingHistory)
	}
	// If there are no rating, or if the new rating is older than the latest one
	if latest, ok := cr.LatestRatings[modelId]; !ok || latest.Year < rating.Year {
		// We set/replace the latest rating
		cr.LatestRatings[modelId] = *rating
	}
	// Add the rating to the history

	if _, ok := cr.AllRatings[modelId]; !ok {
		// No history yet
		all := RatingHistory{
			FromYear: rating.Year,
			ToYear:   rating.Year,
			Grades:   []any{rating.Grade},
			Metrics:  make(map[string][]any, 0),
		}
		for k, v := range rating.Metrics {
			all.Metrics[k] = []any{v}
		}
		cr.AllRatings[modelId] = all
		// And we're done
		return
	}
	// History already present
	all := cr.AllRatings[modelId]
	if rating.Year < all.FromYear {
		diff := rating.Year - all.FromYear
		extendSlices(&all, diff)
		all.FromYear = rating.Year
	} else if rating.Year > all.ToYear {
		diff := rating.Year - all.ToYear
		extendSlices(&all, diff)
		all.ToYear = rating.Year
	}
	index := rating.Year - all.FromYear
	// We do not override prior ratings
	if all.Grades[index] == nil {
		all.Grades[index] = rating.Grade
		for k, v := range rating.Metrics {
			all.Metrics[k][index] = v
		}
	}
	cr.AllRatings[modelId] = all
}

func extendSlices(ratings *RatingHistory, diff int) {
	ratings.Grades = extendSlice(ratings.Grades, diff)
	for k := range ratings.Metrics {
		ratings.Metrics[k] = extendSlice(ratings.Metrics[k], diff)
	}
}

// Extends a slice by the number of items specified.
// if number is negative, the elements are added at the beginning
// if number is positive, they are added at the end
func extendSlice[E any](slice []E, number int) []E {
	more := make([]E, abs(number))
	if number < 0 {
		return append(more, slice...)
	}
	return append(slice, more...)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
