/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"fmt"
	"regexp"

	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
)

// Dependency inversion to avoid exposing expression calculator
type Evaluator func(expression string, parameters map[string]any) any

type Calculator struct {
	Evaluate Evaluator
	Model    ratingmodel.RatingModel
}

func (c *Calculator) Rate(facts facts.Facts, year int) (Rating, error) {
	rating := Rating{}
	rating.Metrics = make(map[string]any, len(c.Model.GlobalMetrics))

	// Setup parameters - one by data model
	// For each data model we have a map of metrics by metric id containing
	// an array with data indexed by year: model.param_name[year]
	params := make(map[string]any, len(facts.Data)+1)
	paramRegexp := make(map[string]*regexp.Regexp, len(params))
	for modelId, modelData := range facts.Data {
		metrics := make(map[string]any)
		for factId, data := range modelData {
			adjustedData := adjustYearRange(data, facts.FromYear, year)
			metrics[factId] = adjustedData

			re, err := regexp.Compile(`(\.` + factId + `(\.[a-z0-9\._]+)?)([^a-z0-9_\[\.]|$)`)
			// We use this to replace with $1[year]$3
			if err != nil {
				log.Error().Msgf("Error: %v", err)
			}
			paramRegexp[factId] = re
		}
		params[modelId] = metrics
	}

	// Add current year
	params["current_year"] = year

	for _, metric := range c.Model.GlobalMetrics {
		v := c.evaluateExpression(metric.Expression, year, paramRegexp, params)
		// add the evaluated metric to the Rating
		rating.Metrics[metric.MetricID] = v
		// Add to the parameters to handle dependencies
		params[metric.MetricID] = v
	}

	for _, grade := range c.Model.Grades {
		// Evaluate all internal metrics first
		// And then evaluate all conditions
		conditionsValid := c.evaluateGrade(grade, year, paramRegexp, params)
		if conditionsValid {
			rating.Grade = grade.Grade
			rating.Year = year
			// First grade meeting the requirements is the one
			break
		}
	}

	if rating.Grade == nil {
		return rating, fmt.Errorf("cannot calculate rating for company %v and year %v", facts.CompanyId, year)
	}
	return rating, nil
}

// This function
func adjustYearRange(data any, numberOfYearPrefix, lastYear int) any {
	switch d := data.(type) {
	case map[string]any:
		for k, v := range d {
			d[k] = adjustYearRange(v, numberOfYearPrefix, lastYear)
		}
		return data
	case []any:
		// We prefix with empty data to be able to access it with the pattern data[2022]
		newArray := append(make([]any, numberOfYearPrefix), d...)
		if len(newArray) < lastYear+1 {
			// If we do not have enough data up to "lastYear"
			newArray = append(newArray, make([]any, lastYear+1-len(newArray))...)
		} else if len(newArray) > lastYear+1 {
			// Data after "lastYear" should be ignored
			newArray = newArray[:lastYear+1]
		}
		return newArray
	default:
		// panic(fmt.Errorf
		log.Error().Msgf("unexpected data type %T: %+v", data, data)
		return data
	}
}

func (c *Calculator) evaluateGrade(grade ratingmodel.Grade, year int, paramRegexp map[string]*regexp.Regexp, params map[string]any) (conditionsValid bool) {
	conditionsValid = true
	//log.Debug().Msgf("Rate - evaluating grade %v", grade)

	defer func() {
		if r := recover(); r != nil {
			log.Info().Msgf("Error while evaluating grade %v: %v", grade.Grade, r)
			conditionsValid = false
		}
	}()

	for _, requirement := range grade.Requirements {

		for _, metric := range requirement.InternalMetrics {
			value := c.evaluateExpression(metric.Expression, year, paramRegexp, params)
			params[metric.MetricID] = value
		}

		for _, condition := range requirement.Conditions {
			value := c.evaluateExpression(condition, year, paramRegexp, params)
			if b, ok := value.(bool); !ok || !b {
				// log.Debug().Msgf("Rate - grade %v requirement condition %v is %v", grade.Grade, condition, value)
				conditionsValid = false
			}
		}
	}
	return
}

func (c *Calculator) evaluateExpression(expression string, year int, paramRegexp map[string]*regexp.Regexp, params map[string]any) any {
	replaceWith := fmt.Sprintf("$1[%v]$3", year)
	for param := range paramRegexp {
		re := paramRegexp[param]
		expression = re.ReplaceAllString(expression, replaceWith)
	}

	v := c.Evaluate(expression, params)
	return v
}
