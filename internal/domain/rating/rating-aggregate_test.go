/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRatingAggregation(t *testing.T) {
	const modelId = "my_model"
	cr := CompanyRating{}

	cr.AddRating(modelId, &Rating{Grade: "A", Year: 2023, Metrics: map[string]any{
		"metric1": 1,
		"metric2": 2,
	}})
	log.Printf("Rating after 2023: %+v", cr)
	assert.Equalf(t, "A", cr.LatestRatings[modelId].Grade, "latest grade")
	assert.Equalf(t, 2023, cr.LatestRatings[modelId].Year, "latest year")
	assert.Equalf(t, 1, cr.LatestRatings[modelId].Metrics["metric1"], "latest metric1")
	assert.Equalf(t, 2, cr.LatestRatings[modelId].Metrics["metric2"], "latest metric2")
	assert.Equalf(t, 1, cr.getMetric(modelId, "metric1", 2023), "2023 metric1")
	cr.AddRating(modelId, &Rating{Grade: "B", Year: 2020, Metrics: map[string]any{
		"metric1": 3,
		"metric2": 4,
	}})
	log.Printf("Rating after 2020: %+v", cr)
	assert.Equalf(t, "A", cr.LatestRatings[modelId].Grade, "latest grade")
	assert.Equalf(t, 2023, cr.LatestRatings[modelId].Year, "latest year")
	assert.Equalf(t, 1, cr.LatestRatings[modelId].Metrics["metric1"], "latest metric1")
	assert.Equalf(t, 2, cr.LatestRatings[modelId].Metrics["metric2"], "latest metric2")
	assert.Equalf(t, "B", cr.getGrade(modelId, 2020), "2020 grade")
	assert.Equalf(t, 1, cr.getMetric(modelId, "metric1", 2023), "2023 metric1")
	assert.Equalf(t, 3, cr.getMetric(modelId, "metric1", 2020), "2020 metric1")

	cr.AddRating(modelId, &Rating{Grade: "C", Year: 2024, Metrics: map[string]any{
		"metric1": 5,
		"metric2": 6,
	}})
	log.Printf("Rating after 2024: %+v", cr)
	assert.Equalf(t, "C", cr.LatestRatings[modelId].Grade, "latest grade")
	assert.Equalf(t, 2024, cr.LatestRatings[modelId].Year, "latest year")
	assert.Equalf(t, 5, cr.LatestRatings[modelId].Metrics["metric1"], "latest metric1")
	assert.Equalf(t, 6, cr.LatestRatings[modelId].Metrics["metric2"], "latest metric2")
	assert.Equalf(t, "B", cr.getGrade(modelId, 2020), "2020 grade")
	assert.Equalf(t, 1, cr.getMetric(modelId, "metric1", 2023), "2023 metric1")
	assert.Equalf(t, 3, cr.getMetric(modelId, "metric1", 2020), "2020 metric1")
	assert.Equalf(t, 6, cr.getMetric(modelId, "metric2", 2024), "2024 metric2")
}
