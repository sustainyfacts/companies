/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
)

type Service interface {
	GetByCompany(companyId string) (CompanyRating, error)
	GetById(id string) (CompanyRating, error)
}

type Repository interface {
	intf.VersionedRepository[CompanyRating]
	FindByCompany(companyId string) (CompanyRating, error)
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo            Repository                                  // Dependency inversion to persistence framework
	evaluate        Evaluator                                   // Dependency inversion to validate framework
	broker          intf.Broker                                 // Messsage broker
	getFacts        func(companyId string) (facts.Facts, error) // Dependency inversion to load Facts
	getRatingModels func() ([]ratingmodel.RatingModel, error)   // Dependency inversion to load rating models
}

var log = logger.Get("rating")

func NewService(repo Repository, broker intf.Broker, getFacts func(companyId string) (facts.Facts, error), getRatingModels func() ([]ratingmodel.RatingModel, error)) Service {
	service := &serviceImpl{repo: repo, broker: broker, getFacts: getFacts, getRatingModels: getRatingModels}
	broker.Subscribe(intf.Topic_CompanyFactsUpdated, service.handleCompanyFactsUpdated)
	return service
}

// Receive
func (s *serviceImpl) handleCompanyFactsUpdated(message any) {
	companyId, ok := message.(string)
	if !ok {
		log.Printf("Error: handleCompanyDisclosureUpdated: message should be of type string. Received %v", message)
		return
	}
	models, err := s.getRatingModels()
	if err != nil {
		log.Printf("Error loading rating models")
		return
	}
	facts, err := s.getFacts(companyId)
	if err != nil {
		log.Printf("Error loading facts for company %v", companyId)
		return
	}
	ratings, err := s.GetByCompany(companyId)
	if err != nil {
		// Create a new rating if does not exist in the first place
		ratings = CompanyRating{CompanyId: companyId, Id: s.repo.GenerateID()}
		ratings.Metadata.OriginalId = ratings.Id
	}

	ratings.update(facts, models, s.evaluate)

	// TODO Updating metadata
	ratings.Metadata.Update("unkown-user-id")

	s.repo.Save(ratings)
}

func (cr *CompanyRating) update(facts facts.Facts, models []ratingmodel.RatingModel, evaluate Evaluator) {
	for _, rm := range models {
		cal := Calculator{Model: rm, Evaluate: evaluate}
		for year := facts.FromYear; year <= facts.ToYear; year++ {
			if rating, err := cal.Rate(facts, year); err == nil {
				cr.AddRating(rm.Id, &rating)
			} else {
				log.Printf("No rating: %v", err)
			}
		}
	}
}

// Implements Service
func (s *serviceImpl) GetById(id string) (CompanyRating, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (CompanyRating, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId
	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetByCompany(companyId string) (CompanyRating, error) {
	return s.repo.FindByCompany(companyId)
}
