/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"encoding/json"
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/mock"
	"go.sustainyfacts.org/companies/api/internal/adapters/evaluator"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
	"gopkg.in/yaml.v2"
)

// Mock for DisclosureService interface
type mocker struct {
	mock.Mock
	Repository
	intf.Broker
}

// Implement interface
func (mock *mocker) getFacts(companyId string) (facts.Facts, error) {
	r := mock.MethodCalled("getFacts", companyId)
	return errorOrNil[facts.Facts](r)
}

// Implement interface
func (mock *mocker) getRatingModels() ([]ratingmodel.RatingModel, error) {
	r := mock.MethodCalled("getRatingModels")
	return errorOrNil[[]ratingmodel.RatingModel](r)
}

// Implement interface
func (mock *mocker) FindByCompany(companyId string) (CompanyRating, error) {
	r := mock.MethodCalled("FindByCompany", companyId)
	return errorOrNil[CompanyRating](r)
}

func errorOrNil[E any](args mock.Arguments) (E, error) {
	if args[1] != nil {
		return args[0].(E), args[1].(error)
	}
	return args[0].(E), nil
}

// Implement interface
func (mock *mocker) Save(rating CompanyRating) error {
	r := mock.MethodCalled("Save", rating)
	if r[0] != nil {
		return r[0].(error)
	}
	return nil
}

func TestHandleFactsUpdated(t *testing.T) {
	testFacts, err := readObjectFromFile[facts.Facts]("test-data/facts_test.json", json.Unmarshal)
	if err != nil {
		t.Errorf("Error reading facts_test.json: %v", err)
	}

	const companyId = "fakecompanyid"
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("getRatingModels").Return([]ratingmodel.RatingModel{testRatingModel}, nil)
	m.On("getFacts", companyId).Return(testFacts, nil)
	m.On("FindByCompany", companyId).Return(CompanyRating{}, nil)
	m.On("Save", mock.Anything).Return(nil)

	service := serviceImpl{repo: m, evaluate: evaluator.Evaluate, broker: m, getFacts: m.getFacts, getRatingModels: m.getRatingModels}

	service.handleCompanyFactsUpdated(companyId)

	m.AssertExpectations(t)
}

func TestRatingModels(t *testing.T) {
	tests := []string{"sf-climate"}

	for _, test := range tests {
		file := "test-data/" + test + "-facts_test.json"
		testFacts, err := readObjectFromFile[facts.Facts](file, json.Unmarshal)
		if err != nil {
			t.Errorf("Error reading %v: %v", file, err)
		}

		file = "test-data/" + test + "-model_test.yaml"
		model, err := readObjectFromFile[ratingmodel.RatingModel](file, yaml.Unmarshal)
		if err != nil {
			t.Errorf("Error reading %v: %v", file, err)
		}

		file = "test-data/" + test + "-rating_test.json"
		expectedRating, err := readObjectFromFile[CompanyRating](file, json.Unmarshal)
		if err != nil {
			t.Errorf("Error reading %v: %v", file, err)
		}

		rating := CompanyRating{}

		rating.update(testFacts, []ratingmodel.RatingModel{model}, evaluator.Evaluate)

		diff := deep.Equal(rating, expectedRating)
		if diff != nil {
			b, _ := json.Marshal(rating)
			t.Errorf("Rating not as expected: %v\nGot: %v", diff, string(b))
		}
	}
}
