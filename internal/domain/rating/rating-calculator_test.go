/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package rating

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.sustainyfacts.org/companies/api/internal/adapters/evaluator"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
)

//go:embed test-data/*
var files embed.FS

var testRatingModel = ratingmodel.RatingModel{
	GlobalMetrics: []ratingmodel.Metric{{MetricID: "som_sum", Expression: "sum(cf.arrayv)"}},
	Grades: []ratingmodel.Grade{
		{Grade: "A", Requirements: []ratingmodel.Requirement{
			{Description: "Something",
				Conditions: []string{
					"som_sum > 12", "sf.intv > 3",
				}},
		}},
		{Grade: "B", Requirements: []ratingmodel.Requirement{
			{Description: "Something less",
				Conditions: []string{
					"som_sum > 8 || sf.intv >= 3",
				}},
		}},
		{Grade: "C"}, // No requirements (catch-all)
	}}

func TestMetricsCalculation(t *testing.T) {
	testFacts, err := readObjectFromFile[facts.Facts]("test-data/facts_test.json", json.Unmarshal)
	if err != nil {
		t.Errorf("Error reading facts_test.json: %v", err)
	}

	const year = 2022
	tests := map[string]struct {
		expression string
		want       any
	}{
		"year":                 {"current_year", year},
		"intv_2022":            {"sf.intv", 5},
		"intv_2021":            {"sf.intv[2021]", 4},
		"objectv_tens_2022":    {"cf.objectv.tens", 50},
		"objectv_tens_2021":    {"cf.objectv.tens[2021]", 40},
		"sum_last_3_intv":      {"sum(sf.intv[-3:])", 12},
		"sum_stuff":            {"10 + sf.intv + sf.floatv", 20.5},
		"intv_2022_square":     {"sf.intv^2 + 33", 58},
		"obj_sum":              {"cf.objectv.tens + cf.objectv.hundreds", 550},
		"obj_count":            {"count(cf.objectv.tens[-5:], # >= 30)", 3},
		"nb_defined":           {"count(sf.incompletev[-5:], # != nil)", 3},
		"sum_array_2022":       {"sum(cf.arrayv)", 15.6},
		"sum_array_last5years": {"lsum(cf.arrayv[-5:])", 48},
		"prefix":               {"sf.prefix", 10},
		"map1plus2":            {"cf.map.c1+cf.map.c2", 3},
	}

	b, _ := json.Marshal(&testFacts)
	fmt.Println(string(b))
	// Initialize global metrics based on test cases above
	globalMetrics := make([]ratingmodel.Metric, 0)
	for i := range tests {
		globalMetrics = append(globalMetrics, ratingmodel.Metric{MetricID: i, Expression: tests[i].expression})
	}

	model := ratingmodel.RatingModel{
		GlobalMetrics: globalMetrics,
		Grades: []ratingmodel.Grade{
			{Grade: "A", Requirements: []ratingmodel.Requirement{
				{Description: "Something"},
			}},
		}}
	calculator := Calculator{Evaluate: evaluator.Evaluate, Model: model}
	rating, _ := calculator.Rate(testFacts, year)

	for metric, test := range tests {
		t.Run("Metric "+metric, func(t *testing.T) {
			result := rating.Metrics[metric]
			assert.Equalf(t, test.want, result, "Metric %v", metric)
		})
	}
}

func TestRequirements(t *testing.T) {
	testFacts, err := readObjectFromFile[facts.Facts]("test-data/facts_test.json", json.Unmarshal)
	if err != nil {
		t.Errorf("Error reading facts_test.json: %v", err)
	}

	calculator := Calculator{Evaluate: evaluator.Evaluate, Model: testRatingModel}
	tests := []struct {
		year  int
		grade string
	}{
		{year: 2022, grade: "A"},
		{year: 2021, grade: "A"},
		{year: 2020, grade: "B"},
		{year: 2019, grade: "C"},
		{year: 2018, grade: "C"},
	}
	for _, test := range tests {
		rating, _ := calculator.Rate(testFacts, test.year)
		if rating.Grade != test.grade {
			t.Errorf(`Rating for %v should be %v, got %v`, test.year, test.grade, rating.Grade)
		}
	}
}

func readObjectFromFile[E any](filename string, unmarshal func(in []byte, out any) error) (E, error) {
	var object E
	bytes, err := fs.ReadFile(files, filename)
	if err != nil {
		return object, err
	}

	err = unmarshal(bytes, &object)
	return object, err
}
