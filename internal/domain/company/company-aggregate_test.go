/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package company

import (
	"strings"
	"testing"

	"github.com/go-playground/validator/v10"
)

// Test Company Validation
func TestCompanyValidationOK(t *testing.T) {
	c := Company{Id: "id", Name: "Sustainyfacts ApS", Country: "DK", NationalID: "12345678"}

	validate := validator.New()
	err := validate.Struct(c)
	if err != nil {
		t.Errorf("Company %v should be valid but got %v", c, err)
	}
}

func TestCompanyValidationError(t *testing.T) {
	// Company with incorrect country, missing national id and invalid Website FQDN
	c := Company{Id: "id", Country: "Denmark", Website: "sustainyfacts(dot)org"}

	validate := validator.New()
	err := validate.Struct(c)

	// Check errors
	if err == nil {
		t.Errorf("Company %v should not be valid", c)
	}
	errorFields := map[string]int{"Country": 0, "NationalID": 0, "Name": 0, "Website": 0}
	valErrs := err.(validator.ValidationErrors)
	if len(valErrs) != len(errorFields) {
		t.Errorf("Company %v should have %v errors but got: %v", c, len(errorFields), valErrs)
	}

	for _, err := range err.(validator.ValidationErrors) {
		errorFields[err.Field()]++
	}
	for field, nbErrors := range errorFields {
		if nbErrors != 1 {
			t.Errorf("Company %v should have an error for field %v but got %v", c, field, valErrs)
		}
	}
}

func TestKeywordGeneration(t *testing.T) {
	// Company with incorrect country, missing national id and invalid Website FQDN
	c := Company{Name: "Sustainyfacts ApS", Description: "We fight greenWashing with facts", Brands: []string{"SustainyFactory", "Sustainify"}}
	c.houseKeeping("fake-user-id")
	expectedKeywords := map[string]int{"sustainyfacts": 1, "aps": 1, "sustainify": 1, "sustainyfactory": 1, "fight": 1, "greenwashing": 1, "with": 1, "facts": 1}

	for _, k := range c.Metadata.Keywords {
		if strings.ToLower(k) != k {
			t.Errorf("Keyword '%v' is not lower case", k)
		} else if expectedKeywords[k] == 0 {
			t.Errorf("Keyword '%v' was not expected", k)
		} else {
			expectedKeywords[k]--
		}
	}
	for k, v := range expectedKeywords {
		if v != 0 {
			t.Errorf("Keyword '%v' was expected but not generated", k)
		}
	}
}

func TestMetadata(t *testing.T) {
	c := Company{Id: "id", Name: "Sustainyfacts ApS", Country: "DK", NationalID: "12345678"}
	c.houseKeeping("fake-user-id")
	if c.Metadata.LastUpdatedTime.IsZero() {
		t.Errorf("lastUpdatedTime not set")
	}
	if c.Metadata.LastUpdatedBy == "" {
		t.Errorf("lastUpdatedBy not set")
	}
}
