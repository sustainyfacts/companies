/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package company

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
)

const testID = "NotSoRandom"

// Mock for CompanyService interface
type mocker struct {
	mock.Mock
	Repository
}

// Implement Repository interface
func (mock *mocker) Save(org Company) error {
	r := mock.Called(org)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

// Implement Repository interface
func (mock *mocker) GenerateID() string {
	return mock.MethodCalled("GenerateID")[0].(string)
}

// Implement Repository interface
func (mock *mocker) Load(id string) (Company, error) {
	r := mock.MethodCalled("Load", id)
	return r[0].(Company), nil
}

// Implement Repository interface
func (mock *mocker) Search(query string, limit int) ([]Company, error) {
	r := mock.MethodCalled("Search", query, limit)
	return r[0].([]Company), r[1].(error)
}

// Implement Validate function
func (mock *mocker) Validate(s interface{}) error {
	r := mock.MethodCalled("Validate", s)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

// Test the creation of an Company
func TestCreateCompany(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("GenerateID").Return(testID)
	m.On("Validate", mock.Anything, mock.Anything).Return(nil)
	m.On("Save", mock.Anything).Return(nil)

	service := serviceImpl{repo: m, validate: m.Validate}

	testCompany := Company{Name: "Sustainyfacts ApS", Country: "DK", NationalID: "12345678"}

	id, err := service.Create(context.TODO(), testCompany)
	if err != nil {
		t.Errorf("Create should not return error but got %v", err)
	}

	// Checks
	if id != testID {
		t.Errorf("Company id should be %v but is %v", testID, id)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
	m.AssertNotCalled(t, "Load")
}

// Test the creation of an Company
func TestCreateInvalidCompany(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("GenerateID").Return(testID)
	m.On("Validate", mock.Anything).Return(errors.New("Invalid Company"))

	testCompany := Company{Name: "Sustainyfacts ApS", Country: "DKK", NationalID: "12345678"}

	service := serviceImpl{repo: m, validate: m.Validate}

	id, err := service.Create(context.TODO(), testCompany)
	if err == nil {
		t.Errorf("Create should have returned an error")
	}

	// Checks
	if id != "" {
		t.Errorf("No company ID should be returned but got %v", id)
	}
	// assert that the expectations were met
	m.AssertExpectations(t)
	m.AssertNotCalled(t, "Load")
	m.AssertNotCalled(t, "Save")
}

func TestUpdateCompany(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("Validate", mock.Anything, mock.Anything).Return(nil)
	m.On("Save", mock.Anything).Return(nil)

	testCompany := Company{Id: "fakeid", Name: "Sustainyfacts ApS", Country: "DK", NationalID: "12345678"}

	service := serviceImpl{repo: m, validate: m.Validate}

	err := service.Update(context.TODO(), testCompany)
	if err != nil {
		t.Errorf("Update should not return error but got %v", err)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
}
