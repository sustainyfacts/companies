/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package company

import (
	"regexp"
	"strings"
	"time"

	"golang.org/x/exp/maps"
)

// Company
type Company struct {
	Id          string   `bson:"_id" validate:"required"`       // auto-generated uid
	Name        string   `validate:"required"`                  // Name of the Company
	Description string   `validate:"omitempty"`                 // A description of the company
	Country     string   `validate:"required,iso3166_1_alpha2"` // Two-letter ISO code of the country.
	NationalID  string   `validate:"required"`                  // Registration ID of the company in corresponding national register
	Icon        string   `validate:"omitempty,url"`             // Icon that can be used in search results
	Ticker      string   `validate:"omitempty"`                 // ISIN: International Securities Identification Numbering system (defined by ISO 6166)
	Website     string   `validate:"omitempty,fqdn"`            // Domain of the company's main web site
	Brands      []string `validate:"omitempty"`                 // Brands owned by the company

	Metadata Metadata `validate:"omitempty"` // Metadata: timestamps, changed by, keywords...
}

// Metadata about a company
type Metadata struct {
	LastUpdatedTime time.Time
	LastUpdatedBy   string

	/* Keywords (for search)
	This is an auto-generated field based on the company's data
	Low case only and single words: they are indexable by mongo
	and searchable by prefix for the live-search */
	Keywords []string
}

func (c Company) Key() string {
	return c.Id
}

const minKeywordLength = 4

func (c *Company) houseKeeping(updatedBy string) {
	keywords := make(map[string]int)

	addWords(keywords, c.Description)
	// Rougth filtering of the noise
	for keyword := range keywords {
		if len(keyword) < minKeywordLength {
			delete(keywords, keyword)
		}
	}
	addWords(keywords, c.Name)
	addWords(keywords, c.Brands...)

	c.Metadata.Keywords = maps.Keys(keywords)
	c.Metadata.LastUpdatedTime = time.Now()
	c.Metadata.LastUpdatedBy = updatedBy

	c.Website = strings.ToLower(c.Website)
}

const o = 1

// Regexp is thread-save
var re = regexp.MustCompile(`\s`)

func addWords(keywords map[string]int, texts ...string) {
	for _, t := range texts {
		for _, v := range re.Split(t, -1) {
			// We only have lower case keywords
			vlow := strings.ToLower(v)
			keywords[vlow] = o
		}
	}
}
