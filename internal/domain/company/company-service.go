/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package company

import (
	"context"
	"errors"

	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

type Service interface {
	intf.CrudService[Company]
	GetByNationalId(country string, nationalId string) (Company, error)
	Search(query string, limit int) ([]Company, error)
}

type Repository interface {
	intf.AggregateRepository[Company]
	FindByCountryAndNationalId(country string, nationalId string) (Company, error)
	Search(query string, limit int) ([]Company, error)
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo     Repository
	validate Validate
}

func NewService(repo Repository, validate Validate) Service {
	return &serviceImpl{repo: repo, validate: validate}
}

func (s *serviceImpl) GetById(id string) (Company, error) {
	return s.repo.Load(id)
}

func (s *serviceImpl) GetByNationalId(country string, nationalId string) (Company, error) {
	return s.repo.FindByCountryAndNationalId(country, nationalId)
}

func (s *serviceImpl) Search(query string, limit int) ([]Company, error) {
	return s.repo.Search(query, limit)
}

func (s *serviceImpl) Update(ctx context.Context, c Company) error {
	if c.Id == "" {
		return errors.New("id must be set")
	}

	err := s.validate(c)
	if err != nil {
		return err
	}
	// We regenerate keywords
	c.houseKeeping(intf.GetUserId(ctx))

	err = s.repo.Save(c)
	if err != nil {
		return err
	}

	return nil
}

func (s *serviceImpl) Create(ctx context.Context, c Company) (string, error) {
	if c.Id != "" {
		return "", errors.New("id must not be set")
	}

	// We give an idea and generate keywords
	c.Id = s.repo.GenerateID()
	c.houseKeeping(intf.GetUserId(ctx))

	err := s.validate(c)
	if err != nil {
		return "", err
	}
	err = s.repo.Save(c)
	if err != nil {
		return "", err
	}
	return c.Id, nil
}

// Dependency inversion to avoid exposing validator
type Validate func(s interface{}) error
