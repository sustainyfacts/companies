/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datamodel

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"strings"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

type Service interface {
	intf.CrudVersionedService[DataModel]
	GetAll(detailled bool) ([]DataModel, error)
	// Validate a list of property ids
	//
	// Property ids are following the pattern model.property.sub_property1
	//
	// This method makes sure that the Datamodel "model" exists
	// and defines the according properties
	//
	// Returns an error if the properties cannot be found in the defined datamodels,
	// and returns nil otherwise
	ValidateProperties(properties []string) error
}

type Repository interface {
	intf.VersionedRepository[DataModel]
	LoadAll() ([]DataModel, error)
	FindAll() ([]DataModel, error) // Returns only overview, without disclosures
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo     Repository
	validate Validate
}

// Dependency inversion to avoid exposing validator
type Validate func(s interface{}) error

func NewService(repo Repository, validate Validate) Service {
	return &serviceImpl{repo: repo, validate: validate}
}

// Implements Service
func (s *serviceImpl) GetById(id string) (DataModel, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (DataModel, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId

	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetAll(detailled bool) ([]DataModel, error) {
	if detailled {
		return s.repo.LoadAll()
	}
	return s.repo.FindAll()
}

// Implements Service
func (s *serviceImpl) ValidateProperties(properties []string) error {
	// Make sure we can load one data model of the other
	sort.Strings(properties)
	var dm DataModel
	var err error
	for _, property := range properties {
		fqn := strings.Split(property, ".")
		if len(fqn) < 2 {
			return fmt.Errorf("Property '%s' doest refer to a datamodel", property)
		}
		propertyId := strings.Join(fqn[1:], ".")
		if dm.Id != fqn[0] {
			// Load relevant datamodel
			dm, err = s.GetById(fqn[0])
			if err != nil {
				return err
			}
		}

		props := dm.Disclosures

		err = findProperty(fqn[1:], props, propertyId, dm.Id)
		if err != nil {
			return err
		}
	}
	return nil
}

// Helper function to find one property in a list of properties
func findProperty(fqn []string, props []Property, propertyId string, datamodelId string) error {
	for depth := 0; depth < len(fqn); depth++ {
		if len(props) == 0 {
			return fmt.Errorf("could not find property %s in datamodel %s", propertyId, datamodelId)
		}
		for _, prop := range props {
			if prop.Id == fqn[depth] {
				if depth == len(fqn)-1 {
					// We have navigated all the way to the FQN of the property
					// which means we have found it
					return nil
				}
				// We proceed further down the property tree
				props = prop.Properties
				continue
			}
		}
	}
	return fmt.Errorf("could not find property %s in datamodel %s", propertyId, datamodelId)
}

// Implements Service
func (s *serviceImpl) Update(ctx context.Context, r DataModel) error {
	if r.Id == "" {
		return errors.New("id must be set")
	}

	// Handle metadata if needed
	if r.Metadata == nil {
		if ori, err := s.repo.Load(r.Id); err == nil {
			r.Metadata = ori.Metadata
		} else {
			r.Metadata = &history.Version{}
		}
	}
	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return err
	}

	err = s.repo.Save(r)
	if err != nil {
		return err
	}

	return nil
}

// Implements Service
func (s *serviceImpl) Create(ctx context.Context, r DataModel) (string, error) {
	if r.Id == "" {
		return "", errors.New("id must be set")
	}

	// Updating metadata
	r.Metadata = &history.Version{}
	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return "", err
	}

	err = s.repo.Save(r)
	if err != nil {
		return "", err
	}
	return r.Id, nil
}
