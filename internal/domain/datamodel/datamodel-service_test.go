/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datamodel

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"
)

const testID = "NotSoRandom"

// Mock for CompanyService interface
type mocker struct {
	mock.Mock
	Repository
}

// Implement Repository interface
func (mock *mocker) Load(id string) (DataModel, error) {
	r := mock.MethodCalled("Load", id)
	if r[1] == nil {
		return r[0].(DataModel), nil
	}
	return DataModel{}, r[1].(error)
}

// Implement Validate function
func (mock *mocker) Validate(s interface{}) error {
	r := mock.MethodCalled("Validate", s)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

// Test the creation of an Company
func TestValidatePropertiesOk(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	dm := loadTestDataModel(t)
	m.On("Load", dm.Id).Return(dm, nil)

	service := serviceImpl{repo: m, validate: m.Validate}

	properties := []string{"sf.tst_001", "sf.tst_005.energy"}

	err := service.ValidateProperties(properties)
	if err != nil {
		t.Errorf("Validate should not fail but got %v", err)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
	// We should load the datamodel only once
	m.AssertNumberOfCalls(t, "Load", 1)
}

func TestValidatePropertiesDatamodelNotFound(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	dm := loadTestDataModel(t)
	m.On("Load", dm.Id).Return(dm, nil)
	m.On("Load", "zzz").Return(nil, fmt.Errorf("Datamodel not found"))

	service := serviceImpl{repo: m, validate: m.Validate}

	properties := []string{"zzz.does.not.exists", "sf.tst_001", "sf.tst_005.energy"}

	err := service.ValidateProperties(properties)
	if err == nil {
		t.Errorf("Validate should have failed")
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
}

func TestValidatePropertiesPropertyNotFound(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	dm := loadTestDataModel(t)
	m.On("Load", dm.Id).Return(dm, nil)

	service := serviceImpl{repo: m, validate: m.Validate}

	properties := []string{"sf.tst_001", "sf.tst_005.energy", "sf.tst_005.nope"}

	err := service.ValidateProperties(properties)
	if err == nil {
		t.Errorf("Validate should have failed because property sf.tst_005.nope does not exist")
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
}
