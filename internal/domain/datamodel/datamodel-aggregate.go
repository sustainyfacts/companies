/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datamodel

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type Property struct {
	Id string `validate:"required,sfid,excludes=." yaml:"id" json:"id"`
	// Public id. We do not use this id for the internal object id in Mongo
	PublicId    string `bson:"public_id,omitempty" yaml:"public_id,omitempty" json:"public_id,omitempty"`
	Name        string `validate:"required" yaml:"name" json:"name"`
	Description string `yaml:"description,omitempty" json:"description,omitempty"`
	// Type: boolean, integer, decimal, string, object, array of `Type`, map of object, enum of string|integer
	Type string `validate:"required,oneof='boolean' 'integer' 'decimal' 'string' 'object' 'array of integer' 'array of decimal' 'array of string' 'array of object' 'map of object' 'enum of integer' 'enum of string' 'calculated'" yaml:"type" json:"type"`
	Unit string `bson:"unit,omitempty" yaml:"unit,omitempty" json:"unit,omitempty"`
	// For type enum, this is the list of possible values, separated by |
	Values string `validate:"required_if=Type 'enum of string',required_if=Type 'enum of integer',omitempty,contains=0x7C" bson:"values,omitempty" yaml:"values,omitempty" json:"values,omitempty"`
	// For type calculated, this field contains the formula
	Formula    string     `validate:"required_if=Type 'calculated',omitempty" bson:"formula,omitempty" yaml:"formula,omitempty" json:"formula,omitempty"`
	Properties []Property `validate:"dive" bson:"properties,omitempty" yaml:"properties,omitempty" json:"properties,omitempty"`
}

type DataModel struct {
	Id string `validate:"required,alphanum,lowercase" bson:"_id" yaml:"id" json:"id"`
	// Public id. We do not use this id for the internal object id in Mongo
	PublicId    string     `validate:"required" bson:"public_id" yaml:"public_id" json:"public_id"`
	Name        string     `validate:"required" yaml:"name" json:"name"`
	Description string     `bson:"description,omitempty" yaml:"description,omitempty" json:"description,omitempty"`
	Version     string     `validate:"required" bson:"version,omitempty" yaml:"version,omitempty" json:"version,omitempty"`
	Disclosures []Property `validate:"dive" yaml:"disclosures,omitempty" json:"disclosures,omitempty"`

	// Metadata: timestamps, changed by
	// Should not be exported/imported in json/yaml
	// This is a pointer because we use direct serialisation also in the API
	Metadata *history.Version `yaml:"-" json:"-" validate:"omitempty"`
}

func (dm DataModel) Key() string {
	return dm.Id
}

func (dm DataModel) NewVersion(newId string) DataModel {
	// Ensure original Id is correct
	dm.Metadata.OriginalId = dm.Id
	return DataModel{Id: newId, PublicId: dm.PublicId,
		Name: dm.Name, Description: dm.Description,
		Version: dm.Version, Disclosures: dm.Disclosures,
		Metadata: dm.Metadata}
}
