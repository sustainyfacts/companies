/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datamodel

import (
	"embed"
	"io/fs"
	"testing"

	"github.com/go-test/deep"
	"go.sustainyfacts.org/companies/api/internal/adapters/utils"
	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"gopkg.in/yaml.v3"
)

//go:embed test-data/*
var embedfs embed.FS

func loadTestDataModel(t *testing.T) DataModel {
	yamlFile, err := fs.ReadFile(embedfs, "test-data/datamodel-test.yaml")
	if err != nil {
		panic("error reading datamodel-test.yaml file")
	}
	var dataModel DataModel
	err = yaml.Unmarshal(yamlFile, &dataModel)
	if err != nil {
		t.Errorf("Error parsing YAML file: %s\n", err)
	}
	return dataModel
}

func TestYaml(t *testing.T) {
	parsed := loadTestDataModel(t)

	declared := DataModel{PublicId: "SFRS", Id: "sf", Name: "Sustainyfacts Reporting Standard",
		Description: "The description of the standard.\nWith several lines.", Version: "0.1",
		Disclosures: []Property{
			{PublicId: "TST-001", Id: "tst_001", Name: "Scope 1 Emissions", Description: "Direct greenhouse gas emissions from owned or controlled sources", Type: "decimal", Unit: "tCO2eq"},
			{PublicId: "TST-002", Id: "tst_002", Name: "Integer Array", Description: "An array of integers", Type: "array of integer", Unit: "FTE"},
			{PublicId: "TST-003", Id: "tst_003", Name: "Decimal Array", Description: "An array of decimals", Type: "array of decimal", Unit: "Wh"},
			{PublicId: "TST-004", Id: "tst_004", Name: "String Array", Description: "An array of strings", Type: "array of string"},
			{PublicId: "TST-005", Id: "tst_005", Name: "Object", Description: "An object with 2 properties", Type: "object", Properties: []Property{
				{PublicId: "TST-005a", Id: "energy", Name: "Energy usage", Description: "Bla bla bla", Type: "decimal", Unit: "MWh"},
				{PublicId: "TST-005b", Id: "percentage", Name: "Percentage of something", Description: "Bla bla bla 2", Type: "integer", Unit: "%"}}},
			{PublicId: "TST-006", Id: "tst_006", Name: "Emissions Reduction Targets", Description: "Targets for reducing greenhouse gas emissions based on SBTi standard", Type: "array of object", Properties: []Property{
				{PublicId: "TST-006a", Id: "target_year", Name: "Target Year", Description: "Year for the target", Type: "integer"},
				{PublicId: "TST-006b", Id: "target_value", Name: "Target Value", Description: "Scope 3 emmissions target", Type: "decimal", Unit: "tCO2eq"}}},
			{PublicId: "TST-007", Id: "enum", Name: "Enum Property", Description: "This is an enum", Type: "enum of string", Values: "First|Second|Thirty-Three|Twenty First|Last"},
			{PublicId: "TST-008", Id: "map", Name: "Map Property", Description: "This is a map of objects", Type: "map of object", Properties: []Property{
				{PublicId: "TST-008a", Id: "a", Name: "A", Description: "A Integer", Type: "integer"},
				{PublicId: "TST-008b", Id: "b", Name: "B", Description: "B Integer", Type: "integer"}},
			}}}

	diff := deep.Equal(parsed, declared)
	if diff != nil {
		t.Error(diff)
	}

	err := utils.Validate(parsed)
	if err != nil {
		t.Errorf("Invalid data model %v", err)
	}

}

func TestValidation(t *testing.T) {
	invalidStandards := []DataModel{
		{ // Invalid: Missing Id
			Name: "Missing Id", Version: "0.1",
			Disclosures: []Property{
				{PublicId: "TST-001", Id: "tst_001",
					Name: "Scope 1 Emissions", Description: "Direct greenhouse gas emissions from owned or controlled sources", Type: "decimal", Unit: "tCO2eq"},
			}},
		// Invalid: uppercase
		getWithProperty(Property{PublicId: "TST-001", Id: "SF_TST_001",
			Name: "Enum", Type: "enum of string", Values: "Option 1,Option 2"}),
		// invalid: "-" character
		getWithProperty(Property{PublicId: "TST-001", Id: "sf-tst_001",
			Name: "Enum", Type: "enum of string", Values: "Option 1,Option 2"}),
		// Enum without |
		getWithProperty(Property{PublicId: "TST-001", Id: "tst_001",
			Name: "Enum", Type: "enum of string", Values: "Option 1,Option 2"}),
		// Enum without values
		getWithProperty(Property{PublicId: "TST-001", Id: "tst_001",
			Name: "Enum", Type: "enum of string"}),
	}

	validStandards := []DataModel{
		getWithProperty(Property{PublicId: "TST-001", Id: "tst_001",
			Name: "Enum", Type: "enum of string", Values: "Option 1|Option 2"}),
	}
	for _, dm := range invalidStandards {
		err := utils.Validate(dm)
		if err == nil {
			t.Errorf("Data model should be invalid %+v", dm)
		}
	}
	for _, dm := range validStandards {
		err := utils.Validate(dm)
		if err != nil {
			t.Errorf("Data model should be valid %+v, got err %+v", dm, err)
		}
	}

}

func getWithProperty(property Property) DataModel {
	return DataModel{
		Id: "sfrs", PublicId: "SFRS",
		Name: "With property", Version: "0.1",
		Disclosures: []Property{property}}
}

func TestVersioning(t *testing.T) {
	parsed := loadTestDataModel(t)
	parsed.Metadata = &history.Version{}

	newVersion := parsed.NewVersion("newfakeid")
	if newVersion.Key() != "newfakeid" {
		t.Error("new version id should be 'newfakeid'")
	}

	// To be able to compare we need to restore the Id
	newVersion.Id = parsed.Id
	diff := deep.Equal(parsed, newVersion)
	if diff != nil {
		t.Error(diff)
	}
}
