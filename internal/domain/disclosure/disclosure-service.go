/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package disclosure

import (
	"context"
	"errors"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

type Service interface {
	intf.CrudVersionedService[Disclosure]
	GetByCompany(companyId string) ([]Disclosure, error)
	GetPublishedByCompany(companyId string) ([]Disclosure, error)
}

type Repository interface {
	intf.VersionedRepository[Disclosure]
	FindByCompany(companyId string) ([]Disclosure, error)
	FindPublishedByCompany(companyId string) ([]Disclosure, error)
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo      Repository
	validate  Validate
	publisher intf.Publisher
}

// Dependency inversion to avoid exposing validator
type Validate func(s interface{}) error

func NewService(repo Repository, validate Validate, publisher intf.Publisher) Service {
	return &serviceImpl{repo: repo, validate: validate, publisher: publisher}
}

// Implements Service
func (s *serviceImpl) GetById(id string) (Disclosure, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (Disclosure, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId
	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetByCompany(companyId string) ([]Disclosure, error) {
	return s.repo.FindByCompany(companyId)
}

func (s *serviceImpl) GetPublishedByCompany(companyId string) ([]Disclosure, error) {
	return s.repo.FindPublishedByCompany(companyId)
}

// Implements Service
func (s *serviceImpl) Update(ctx context.Context, r Disclosure) error {
	if r.Id == "" {
		return errors.New("id must be set")
	}

	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return err
	}

	err = s.repo.Save(r)
	if err != nil {
		return err
	}

	// Notify only if the disclosure is published
	if r.IsPublished {
		s.publisher.Publish(intf.Topic_CompanyDisclosureUpdated, r.CompanyId)
	}

	return nil
}

// Implements Service
func (s *serviceImpl) Create(ctx context.Context, r Disclosure) (string, error) {
	if r.Id != "" {
		return "", errors.New("id must not be set")
	}

	// We give an idea and generate keywords
	r.Id = s.repo.GenerateID()
	r.Metadata.OriginalId = r.Id

	// Updating metadata
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return "", err
	}

	err = s.repo.Save(r)
	if err != nil {
		return "", err
	}

	// Notify only if the disclosure is published
	if r.IsPublished {
		s.publisher.Publish(intf.Topic_CompanyDisclosureUpdated, r.CompanyId)
	}

	return r.Id, nil
}
