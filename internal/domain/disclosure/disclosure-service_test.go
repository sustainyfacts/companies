/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package disclosure

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
)

// Mock for DisclosureService interface
type mocker struct {
	mock.Mock
	Repository
}

// Implement Repository interface
func (mock *mocker) GenerateID() string {
	return mock.MethodCalled("GenerateID")[0].(string)
}

// Implement Repository interface
func (mock *mocker) Save(org Disclosure) error {
	r := mock.MethodCalled("Save", org)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

// Implement Repository interface
func (mock *mocker) Load(id string) (Disclosure, error) {
	r := mock.MethodCalled("Load", id)
	return r[0].(Disclosure), nil
}

// Implement Repository interface
func (mock *mocker) Search(query string, limit int) ([]Disclosure, error) {
	r := mock.MethodCalled("Search", query, limit)
	return r[0].([]Disclosure), r[1].(error)
}

// Implement Validate function
func (mock *mocker) Validate(s interface{}) error {
	r := mock.MethodCalled("Validate", s)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

func (mock *mocker) Publish(topic string, message any) {
	mock.MethodCalled("Publish", topic, message)
}

// Test Data
const (
	testCompanyId = "sustainyfakeid" // company id
	testID        = "NotSoRandom"    // disclosure id
)

// Test the creation of an Disclosure
func TestCreateDisclosure(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("GenerateID").Return(testID)
	m.On("Validate", mock.Anything, mock.Anything).Return(nil)
	m.On("Save", mock.Anything).Return(nil)
	m.On("Publish", "company_disclosure_updated", testCompanyId)

	service := serviceImpl{repo: m, validate: m.Validate, publisher: m}

	testDisclosure := Disclosure{CompanyId: testCompanyId, Currency: "DKK", Year: 2023, IsPublished: true}

	id, err := service.Create(context.TODO(), testDisclosure)
	if err != nil {
		t.Errorf("Create should not return error but got %v", err)
	}

	// Checks
	if id != testID {
		t.Errorf("Disclosure id should be %v but is %v", testID, id)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
	m.AssertNotCalled(t, "Load")
}

// Test the creation of an Disclosure
func TestCreateInvalidDisclosure(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("GenerateID").Return(testID)
	m.On("Validate", mock.Anything).Return(errors.New("Invalid Disclosure"))

	testDisclosure := Disclosure{CompanyId: "sustainyfakeid", Currency: "DK", Year: 2023}

	service := serviceImpl{repo: m, validate: m.Validate}

	id, err := service.Create(context.TODO(), testDisclosure)
	if err == nil {
		t.Errorf("Create should have returned an error")
	}

	// Checks
	if id != "" {
		t.Errorf("No Disclosure ID should be returned but got %v", id)
	}
	// assert that the expectations were met
	m.AssertExpectations(t)
	m.AssertNotCalled(t, "Load")
	m.AssertNotCalled(t, "Save")
}

func TestUpdateDisclosure(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("Validate", mock.Anything, mock.Anything).Return(nil)
	m.On("Save", mock.Anything).Return(nil)

	testDisclosure := Disclosure{Id: "disclosure007", CompanyId: testCompanyId, Currency: "DKK", Year: 2023, IsPublished: false}

	service := serviceImpl{repo: m, validate: m.Validate}

	err := service.Update(context.TODO(), testDisclosure)
	if err != nil {
		t.Errorf("Update should not return error but got %v", err)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
}

func TestUpdatePublishedDisclosure(t *testing.T) {
	// Setup Mock and expected behavior
	m := &mocker{}
	m.On("Validate", mock.Anything, mock.Anything).Return(nil)
	m.On("Save", mock.Anything).Return(nil)
	m.On("Publish", "company_disclosure_updated", testCompanyId)

	testDisclosure := Disclosure{Id: "disclosure007", CompanyId: testCompanyId, Currency: "DKK", Year: 2023, IsPublished: true}

	service := serviceImpl{repo: m, validate: m.Validate, publisher: m}

	err := service.Update(context.TODO(), testDisclosure)
	if err != nil {
		t.Errorf("Update should not return error but got %v", err)
	}

	// assert that the expectations were met
	m.AssertExpectations(t)
}
