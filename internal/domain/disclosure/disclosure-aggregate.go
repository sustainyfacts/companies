/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package disclosure

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type Disclosure struct {
	Id          string                    `bson:"_id" validate:"required"`                          // auto-generated uid
	CompanyId   string                    `validate:"required" bson:"company_id" json:"company_id"` // uid of the Company
	Type        Type                      `validate:"required"`                                     // Type of the disclosure: Sustainability Report, Annual Report
	IsPublished bool                      `bson:"is_published" json:"is_published"`                 // Status of the disclosure: draft/published
	Year        int                       `validate:"required"`                                     // Year covered by the disclosure
	Currency    string                    `validate:"required,iso4217"`                             // The currency used in the disclosure
	Url         string                    `validate:"omitempty,url"`                                // URL where the disclosure can be downloaded from
	Standards   []string                  `validate:"omitempty"`                                    // Disclosureing Standards used by the disclosure
	Data        map[string]map[string]any `validate:"omitempty"`                                    // Data and Metrics

	Metadata history.Version `validate:"omitempty"` // Metadata: timestamps, changed by
}

type Type string

const (
	SustainabilityReport, AnnualReport, CSRReport Type = "sustainability_report",
		"annual_report", "csr_report" // Types
)

func (d Disclosure) Key() string {
	return d.Id
}

func (d Disclosure) NewVersion(newid string) Disclosure {
	// Ensure original Id is correct
	d.Metadata.OriginalId = d.Id
	return Disclosure{Id: newid, CompanyId: d.CompanyId, Type: d.Type, IsPublished: d.IsPublished,
		Year: d.Year, Currency: d.Currency, Url: d.Url, Standards: d.Standards, Data: d.Data, Metadata: d.Metadata}
}
