/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package dataset

import (
	"context"
	"embed"
	"encoding/json"
	"errors"
	"io/fs"
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/mock"
)

// Mock for Repository interface
type mocker struct {
	mock.Mock
	Repository
}

// Implement Repository interface
func (mock *mocker) GenerateID() string {
	return mock.MethodCalled("GenerateID")[0].(string)
}

// Implement Repository interface
func (mock *mocker) Save(org Dataset) error {
	r := mock.MethodCalled("Save", org)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

// Implement Repository interface
func (mock *mocker) Load(id string) (Dataset, error) {
	r := mock.MethodCalled("Load", id)
	return r[0].(Dataset), nil
}

// Implement Repository interface
func (mock *mocker) Search(query string, limit int) ([]Dataset, error) {
	r := mock.MethodCalled("Search", query, limit)
	return r[0].([]Dataset), r[1].(error)
}

// Implement Validate function
func (mock *mocker) Validate(s any) error {
	r := mock.MethodCalled("Validate", s)[0]
	if r != nil {
		return r.(error)
	}
	return nil
}

func (mock *mocker) Publish(topic string, message any) {
	mock.Called(topic, message)
}

func readObjectFromFile[E any](filename string) (E, error) {
	var object E
	bytes, err := fs.ReadFile(files, filename)
	if err != nil {
		return object, err
	}

	err = json.Unmarshal(bytes, &object)
	return object, err
}

//go:embed test-data/*
var files embed.FS

// Test Data
const (
	testID = "NotSoRandom" // dataset id
)

func TestDatasetFromJson(t *testing.T) {
	dataset, err := readObjectFromFile[Dataset]("test-data/dataset_test.json")
	if err != nil {
		t.Errorf("Error reading dataset_test.json: %v", err)
	}
	expectedDataset := Dataset{Id: "uid-for-dataset", IsPublished: false, ModelId: "cdp_climate",
		Years: []int{2022, 2023}, Source: "CDP", SourceUrl: "https://www.cdp.net",
		Standards: []string{"ghg", "sbti"}, DisclosureCount: 135, FileService: "local",
		FileLocation: "/data", FileId: "path/to/dataset.csv"}

	diff := deep.Equal(dataset, expectedDataset)
	if diff != nil {
		b, _ := json.Marshal(dataset)
		t.Errorf("Dataset not as expected: %v\nGot: %v", diff, string(b))
	}
}

// Test the creation of an Dataset
func TestCreateDataset(t *testing.T) {
	// Setup Mock and expected behavior
	mocks := &mocker{}
	mocks.On("GenerateID").Return(testID)
	mocks.On("Validate", mock.Anything).Return(nil)
	mocks.On("Save", mock.Anything).Return(nil)
	mocks.On("Publish", "dataset_updated", testID)

	service := serviceImpl{repo: mocks, validate: mocks.Validate, publisher: mocks}

	testDataset := Dataset{Years: []int{2022, 2023}, IsPublished: true}

	id, err := service.Create(context.TODO(), testDataset)
	if err != nil {
		t.Errorf("Create should not return error but got %v", err)
	}

	// Checks
	if id != testID {
		t.Errorf("Dataset id should be %v but is %v", testID, id)
	}

	// assert that the expectations were met
	mocks.AssertExpectations(t)
	mocks.AssertNotCalled(t, "Load")
}

// Test the creation of a Dataset
func TestCreateInvalidDataset(t *testing.T) {
	// Setup Mock and expected behavior
	mocks := &mocker{}
	mocks.On("GenerateID").Return(testID)
	mocks.On("Validate", mock.Anything).Return(errors.New("invalid dataset"))

	testDataset := Dataset{Years: []int{2022, 2023}}

	service := serviceImpl{repo: mocks, validate: mocks.Validate}

	id, err := service.Create(context.TODO(), testDataset)
	if err == nil {
		t.Errorf("Create should have returned an error")
	} else if err.Error() != "invalid dataset" {
		t.Errorf("Expected error 'invalid dataset' but got '%v'", err)
	}

	// Checks
	if id != "" {
		t.Errorf("No Dataset ID should be returned but got %v", id)
	}
	// assert that the expectations were met
	mocks.AssertExpectations(t)
	mocks.AssertNotCalled(t, "Load")
	mocks.AssertNotCalled(t, "Save")
}

func TestUpdateDataset(t *testing.T) {
	// Setup Mock and expected behavior
	mocks := &mocker{}
	mocks.On("Validate", mock.Anything).Return(nil)
	mocks.On("Save", mock.Anything).Return(nil)

	testDataset := Dataset{Id: "dataset007", Years: []int{2022, 2023}, IsPublished: false}

	service := serviceImpl{repo: mocks, validate: mocks.Validate}

	err := service.Update(context.TODO(), testDataset)
	if err != nil {
		t.Errorf("Update should not return error but got %v", err)
	}

	// assert that the expectations were met
	mocks.AssertExpectations(t)
}

func TestUpdatePublishedDataset(t *testing.T) {
	// Setup Mock and expected behavior
	mocks := &mocker{}
	mocks.On("Validate", mock.Anything).Return(nil)
	mocks.On("Save", mock.Anything).Return(nil)
	mocks.On("Publish", "dataset_updated", "dataset007")

	testDataset := Dataset{Id: "dataset007", Years: []int{2022, 2023}, IsPublished: true}

	service := serviceImpl{repo: mocks, validate: mocks.Validate, publisher: mocks}

	err := service.Update(context.TODO(), testDataset)
	if err != nil {
		t.Errorf("Update should not return error but got %v", err)
	}

	// assert that the expectations were met
	mocks.AssertExpectations(t)
}
