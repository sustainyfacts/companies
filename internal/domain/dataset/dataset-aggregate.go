/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package dataset

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type Dataset struct {
	Id              string          `bson:"_id" validate:"required"`                                      // uid
	IsPublished     bool            `bson:"is_published" json:"is_published"`                             // Status of the disclosure: draft/published
	ModelId         string          `validate:"required" bson:"model_id" json:"model_id"`                 // Mapping configuration to extract disclosures
	Years           []int           `validate:"required"`                                                 // Years covered by the dataset
	Source          string          `validate:"omitempty"`                                                // Source of the data (an organisation, a database )
	SourceUrl       string          `validate:"omitempty,url" bson:"source_url" json:"source_url"`        // Website or URL for the source of the data
	FileService     string          `validate:"required" bson:"file_service" json:"file_service"`         // File storage service used. For example "local" for local file system storage
	FileLocation    string          `validate:"required" bson:"file_location" json:"file_location"`       // Location within storage service used. Could be the folder for "local" or a bucket ref for "s3".
	FileId          string          `validate:"required" bson:"file_id" json:"file_id"`                   // Id/Path/Url of the file with the Storage service and location
	Standards       []string        `validate:"omitempty"`                                                // Disclosure Standards used in the dataset
	DisclosureCount int             `validate:"required" bson:"disclosure_count" json:"disclosure_count"` // Count of disclosures in the dataset (1 disclosure per company and per year)
	Metadata        history.Version `validate:"omitempty"`                                                // Metadata: timestamps, changed by
}

func (d Dataset) Key() string {
	return d.Id
}

func (d Dataset) NewVersion(newid string) Dataset {
	// Ensure original Id is correct
	d.Metadata.OriginalId = d.Id
	return Dataset{Id: newid, ModelId: d.ModelId, Years: d.Years, Source: d.Source,
		SourceUrl: d.SourceUrl, Standards: d.Standards, DisclosureCount: d.DisclosureCount, Metadata: d.Metadata}
}
