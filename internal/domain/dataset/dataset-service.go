/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package dataset

import (
	"context"
	"errors"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
)

type Service interface {
	intf.CrudVersionedService[Dataset]
}

type Repository interface {
	intf.VersionedRepository[Dataset]
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo      Repository
	validate  Validate
	publisher intf.Publisher
}

// Dependency inversion to avoid exposing validator
type Validate func(s interface{}) error

func NewService(repo Repository, validate Validate, publisher intf.Publisher) Service {
	return &serviceImpl{repo: repo, validate: validate, publisher: publisher}
}

// Implements Service
func (s *serviceImpl) GetById(id string) (Dataset, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (Dataset, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId
	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) Update(ctx context.Context, dataset Dataset) error {
	if dataset.Id == "" {
		return errors.New("id must be set")
	}

	dataset.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(dataset)
	if err != nil {
		return err
	}

	err = s.repo.Save(dataset)
	if err != nil {
		return err
	}

	// Notify only if the dataset is published
	if dataset.IsPublished {
		s.publisher.Publish(intf.Topic_DatasetUpdated, dataset.Id)
	}

	return nil
}

// Implements Service
func (s *serviceImpl) Create(ctx context.Context, dataset Dataset) (string, error) {
	if dataset.Id != "" {
		return "", errors.New("id must not be set")
	}

	// We give an idea and generate keywords
	dataset.Id = s.repo.GenerateID()
	dataset.Metadata.OriginalId = dataset.Id

	// Updating metadata
	dataset.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(dataset)
	if err != nil {
		return "", err
	}

	err = s.repo.Save(dataset)
	if err != nil {
		return "", err
	}

	// Notify only if the dataset is published
	if dataset.IsPublished {
		s.publisher.Publish(intf.Topic_DatasetUpdated, dataset.Id)
	}

	return dataset.Id, nil
}
