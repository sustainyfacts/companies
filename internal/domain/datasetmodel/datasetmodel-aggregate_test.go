/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datasetmodel

import (
	"embed"
	"encoding/json"
	"io/fs"
	"testing"

	"github.com/go-test/deep"
	"go.sustainyfacts.org/companies/api/internal/adapters/utils"
)

func TestValidate(t *testing.T) {
	dsm := DatasetModel{Id: "cdp_mapper",
		Format: "csv", Mappings: map[string]string{
			"cdp_company": "company.name",
			"cdp_year":    "year",
		}}
	err := utils.Validate(dsm)
	if err != nil {
		t.Errorf("Dataset model should be valid %v, but got error %v", dsm, err)
	}
}
func TestMappingsFromJson(t *testing.T) {
	model, err := readObjectFromFile[DatasetModel]("test-data/datasetmodel_test.json")
	if err != nil {
		t.Errorf("Error reading dataset_mappings_test.json: %v", err)
	}
	expectedModel := DatasetModel{Id: "cdp_mapper",
		Format: "csv", Mappings: map[string]string{
			"cdp_company":         "company.name",
			"cdp_year":            "year",
			"cdp_scope1":          "ghg.scope1",
			"cdp_scope2_market":   "ghg.scope2.market",
			"cdp_scope2_location": "ghg.scope2.location",
			"cdp_scope3":          "ghg.scope3",
		}}

	diff := deep.Equal(model, expectedModel)
	if diff != nil {
		b, _ := json.Marshal(model)
		t.Errorf("Model not as expected: %v\nGot: %v", diff, string(b))
	}
}

func readObjectFromFile[E any](filename string) (E, error) {
	var object E
	bytes, err := fs.ReadFile(files, filename)
	if err != nil {
		return object, err
	}

	err = json.Unmarshal(bytes, &object)
	return object, err
}

//go:embed test-data/*
var files embed.FS
