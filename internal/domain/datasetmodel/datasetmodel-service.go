/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datasetmodel

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strconv"

	"go.sustainyfacts.org/companies/api/internal/domain/history"
	"go.sustainyfacts.org/companies/api/internal/domain/intf"
	"golang.org/x/exp/maps"
)

func (s *serviceImpl) GetFactReaderFactory(modelId string) (FactReaderFactory, error) {
	model, err := s.repo.Load(modelId)
	if err != nil {
		return nil, err
	}
	switch model.Format {
	case "csv":
		return getFactReaderFactory(s.csvReaderFactory, model.Mappings)
	}

	return nil, fmt.Errorf("unsupported format: %s", model.Format)
}

// This method takes a CSVReaderFactory and mappings, and returns a FactReaderFactory.
//
// The reason for its existence is to reverse dependencies towards the CSVReader, and also
// present an easy way to consume the facts, that is abstracting from
// the underlying format (CSV in this case, could be something else in the future)
func getFactReaderFactory(csvReaderFactory CSVReaderFactory, mapping map[string]string) (FactReaderFactory, error) {
	// The factory is a method that returns a reader
	factoryFunc := func(reader io.Reader) (FactReader, error) {
		// Where we read from (CSV)
		cvsReader := csvReaderFactory.GetReader(reader)

		// Read the column names
		headers, err := cvsReader.Read()
		if err != nil {
			return nil, err
		}

		// The factory is a method that returns a Reader
		readerFunc := func() (map[string]any, error) {
			values, err := cvsReader.Read()
			record := map[string]any{}
			if err != nil {
				return nil, err
			}
			for i, value := range values {
				key := mapping[headers[i]]
				record[key] = intFloatOrString(value)
			}
			return record, nil
		}

		return factReaderFunc(readerFunc), nil
	}
	return factReaderFactoryFunc(factoryFunc), nil
}

// Converts to an int, float64 or string
func intFloatOrString(value string) any {
	if v, err := strconv.Atoi(value); err == nil {
		return v
	} else if v, err := strconv.ParseFloat(value, 64); err == nil {
		return v
	}
	return value
}

type factReaderFactoryFunc func(reader io.Reader) (FactReader, error)

func (f factReaderFactoryFunc) GetReader(reader io.Reader) (FactReader, error) {
	return f(reader)
}

type factReaderFunc func() (map[string]any, error)

func (f factReaderFunc) Read() (map[string]any, error) {
	return f()
}

// Implements Service
func (s *serviceImpl) GetById(id string) (DatasetModel, error) {
	return s.repo.Load(id)
}

// Implements Service
func (s *serviceImpl) GetByVersion(id string, version int) (DatasetModel, error) {
	r, err := s.repo.LoadVersion(id, version)
	if err != nil {
		return r, err
	}
	// We override the id to return the document exactly as it used to be
	r.Id = r.Metadata.OriginalId

	return r, err
}

// Implements Service
func (s *serviceImpl) GetVersions(id string) ([]history.Version, error) {
	return s.repo.FindVersions(id)
}

// Implements Service
func (s *serviceImpl) GetAll(detailled bool) ([]DatasetModel, error) {
	if detailled {
		return s.repo.LoadAll()
	}
	return s.repo.FindAll()
}

// Implements Service
func (s *serviceImpl) Update(ctx context.Context, r DatasetModel) error {
	if r.Id == "" {
		return errors.New("id must be set")
	}

	// Handle metadata if needed
	if r.Metadata == nil {
		if ori, err := s.repo.Load(r.Id); err == nil {
			r.Metadata = ori.Metadata
		} else {
			r.Metadata = &history.Version{}
		}
	}

	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return err
	}

	err = s.validateProperties(maps.Values(r.Mappings))
	if err != nil {
		return err
	}

	err = s.repo.Save(r)
	if err != nil {
		return err
	}

	return nil
}

// Implements Service
func (s *serviceImpl) Create(ctx context.Context, r DatasetModel) (string, error) {
	if r.Id == "" {
		return "", errors.New("id must be set")
	}

	// Updating metadata
	r.Metadata = &history.Version{}
	r.Metadata.OriginalId = r.Id
	r.Metadata.Update(intf.GetUserId(ctx))

	err := s.validate(r)
	if err != nil {
		return "", err
	}

	err = s.validateProperties(maps.Values(r.Mappings))
	if err != nil {
		return "", err
	}

	err = s.repo.Save(r)
	if err != nil {
		return "", err
	}
	return r.Id, nil
}

// -------------------------------
// Interfaces and type definitions
// -------------------------------

type Service interface {
	intf.CrudVersionedService[DatasetModel]
	GetAll(detailled bool) ([]DatasetModel, error)
	GetFactReaderFactory(modelId string) (FactReaderFactory, error)
}

type Repository interface {
	intf.VersionedRepository[DatasetModel]
	LoadAll() ([]DatasetModel, error)
	FindAll() ([]DatasetModel, error) // Returns only overview, without disclosures
}

// Types used here must be thread-safe
type serviceImpl struct {
	repo               Repository
	validate           Validate
	validateProperties ValidateProperties
	csvReaderFactory   CSVReaderFactory
}

// -------------------------------
// Reversal of dependencies
// -------------------------------
type Validate func(s interface{}) error

type ValidateProperties func(properties []string) error

type FactReader interface {
	// Returns of map where keys are fq ids (ghg.scope1) and values are the datapoint
	Read() (record map[string]any, err error)
}

type FactReaderFactory interface {
	// Returns a parser for the file type and a reader
	GetReader(reader io.Reader) (FactReader, error)
}

type CSVReader interface {
	Read() (record []string, err error)
}

type CSVReaderFactory interface {
	GetReader(reader io.Reader) CSVReader
}

func NewService(repo Repository, validate Validate, validateProperties ValidateProperties, csvReaderFactory CSVReaderFactory) Service {
	return &serviceImpl{repo: repo, validate: validate, validateProperties: validateProperties, csvReaderFactory: csvReaderFactory}
}
