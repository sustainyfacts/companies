/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datasetmodel

import (
	"go.sustainyfacts.org/companies/api/internal/domain/history"
)

type DatasetModel struct {
	Id     string `bson:"_id" validate:"required,sfid,excludes=."` // uid
	Format string `validate:"required,eq=csv"`                     // File format: csv
	// Mappings between file data and facts
	Mappings map[string]string `validate:"required,has_values=company.*;year,dive,sfid" yaml:"mappings,omitempty" json:"mappings,omitempty"`

	// Metadata: timestamps, changed by
	// Should not be exported/imported in json/yaml
	// This is a pointer because we use direct serialisation also in the API
	Metadata *history.Version `yaml:"-" json:"-" validate:"omitempty"`
}

func (m DatasetModel) Key() string {
	return m.Id
}

func (m DatasetModel) NewVersion(newid string) DatasetModel {
	// Ensure original Id is correct
	m.Metadata.OriginalId = m.Id
	return DatasetModel{Id: newid, Format: m.Format, Mappings: m.Mappings, Metadata: m.Metadata}
}
