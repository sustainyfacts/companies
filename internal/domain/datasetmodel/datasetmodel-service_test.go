/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package datasetmodel

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/fs"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type csvReaderFactoryFunc func(reader io.Reader) CSVReader

// A little gymastic to be able to use an anonymous function to wrap csv.NewReader
func (f csvReaderFactoryFunc) GetReader(reader io.Reader) CSVReader { return f(reader) }

func TestFactoryTransformer(t *testing.T) {
	csvReaderFactory := csvReaderFactoryFunc(func(reader io.Reader) CSVReader { return csv.NewReader(reader) })
	mapping := map[string]string{"cdp_company": "company.name",
		"cdp_year":            "year",
		"cdp_scope1":          "ghg.scope1",
		"cdp_scope2_market":   "ghg.scope2.market",
		"cdp_scope2_location": "ghg.scope2.location",
		"cdp_scope3":          "ghg.scope3"}

	factory, _ := getFactReaderFactory(csvReaderFactory, mapping)

	input, err := fs.ReadFile(files, "test-data/datasetmodel_test.csv")
	assert.NoError(t, err)

	reader, err := factory.GetReader(strings.NewReader(string(input)))
	assert.NoError(t, err)

	expected := []map[string]any{
		{
			"company.name":        "Novo Nordisk",
			"ghg.scope1":          76000,
			"ghg.scope2.location": 131332,
			"ghg.scope2.market":   15081,
			"ghg.scope3":          1150630,
			"year":                2022},
		{
			"company.name":        "Ørsted",
			"ghg.scope1":          2510000,
			"ghg.scope2.location": 45000,
			"ghg.scope2.market":   1000,
			"ghg.scope3":          11000000,
			"year":                2022},
		{
			"company.name":        "Novo Nordisk",
			"ghg.scope1":          77662,
			"ghg.scope2.location": 138215,
			"ghg.scope2.market":   14629,
			"ghg.scope3":          2041000,
			"year":                2023},
	}
	for i := range expected {
		first, err := reader.Read()
		assert.NoError(t, err)
		assert.Equal(t, expected[i], first, fmt.Sprintf("Row %d is incorrect", i+1))
	}
}
