# Data Platform API

The Sustainyfacts data platform API is an service to maintain a list of companies and related sustainability data that Sustainyfacts keeps track of.

## License

Identity is licensed under the GNU Affero General Public License - See LICENSE file and list of authors in AUTHOR file.

## Configuration

### API Service

The service uses the following environment variables:

| Variable          | Example                                      | Description
|-------------------|----------------------------------------------|------------
| `API_URL`         | `http://data-api.sustainyfacts.localhost`    | URL from which the API is served
| `UI_URL`          | `http://app.sustainyfacts.localhost`         | URL from which the UI is served
| `JWT_CERTS_URL`   | `https://sustainyfacts-8svmnf.zitadel.cloud/oauth/v2/keys`         | URL of the certificates issuing the JWT tokens
| `MONGO_URI`       | `mongodb://mongo`                            | Connect URI for Mongo
| `MONGO_DB`        | `companiesdb`                                | Name of mongo database

## API end-points

### Documentation

See http://data-api.sustainyfacts.localhost/docs/ for API documentation. The documentation is generated with swaggo, and it can be updated by running `swag init`.

| End-point         | Description
|-------------------|------------
| `/ping`           | Hearbeat, returns "."
| `/companies`      | Create and get companies
| `/disclosures`    | Create and get disclosures (data about companies)
| `/facts`          | Create and get facts (verified, published data about companies)
| `/ratings`        | Create and get ratings (evaluation of companies based on facts)
| `/datamodels`     | Administration of data models
| `/datasetmodels`  | Administration of dataset models
| `/ratingmodels`   | Administration of rating models

### Manual Testing

You can use Insomnia to test the API by importing the file `insomnia.yaml` into a new personal project.