# Copyright © 2023 The Authors (See AUTHORS file)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

FROM golang:1.22-alpine as build

# Bash for easier debugging on live container
RUN apk add --no-cache --update \
	bash \
	&& rm -rf /tmp/* /var/cache/apk/*

WORKDIR /build

ENV CGO_ENABLED=0
COPY go.mod go.sum ./
RUN go mod download
RUN go mod verify

COPY . .
RUN go build -ldflags="-s -w" -o /tmp/api

#RUN go test ./...

FROM build as dev

RUN go install github.com/githubnemo/CompileDaemon@latest
WORKDIR /build
EXPOSE 80
ENTRYPOINT ["/go/bin/CompileDaemon", "-color", "-include", "go.mod", "--graceful-kill", "-command", "/tmp/api", "-build", "go build -o /tmp/api"]

FROM alpine as prod
RUN apk add --no-cache --update \
	ca-certificates \
    && update-ca-certificates \
	&& rm -rf /tmp/* /var/cache/apk/*
WORKDIR /app
COPY --from=build /tmp/api .
EXPOSE 80
ENTRYPOINT ["/app/api"]
