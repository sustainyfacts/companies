/*
Copyright © 2023 The Authors (See AUTHORS file)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/sethvargo/go-envconfig"
	"go.sustainyfacts.org/companies/api/internal/adapters/broker"
	"go.sustainyfacts.org/companies/api/internal/adapters/companyapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/datamodelapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/datasetmodelapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/disclosureapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/factsapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/logger"
	"go.sustainyfacts.org/companies/api/internal/adapters/ratingapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/ratingmodelapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/repository"
	"go.sustainyfacts.org/companies/api/internal/adapters/restapi"
	"go.sustainyfacts.org/companies/api/internal/adapters/utils"
	"go.sustainyfacts.org/companies/api/internal/domain/company"
	"go.sustainyfacts.org/companies/api/internal/domain/datamodel"
	"go.sustainyfacts.org/companies/api/internal/domain/datasetmodel"
	"go.sustainyfacts.org/companies/api/internal/domain/disclosure"
	"go.sustainyfacts.org/companies/api/internal/domain/facts"
	"go.sustainyfacts.org/companies/api/internal/domain/rating"
	"go.sustainyfacts.org/companies/api/internal/domain/ratingmodel"
)

var (
	Version = "0.2"

	logo = strings.ReplaceAll(`
     _____            __        _             ____           __      
    / ___/__  _______/ /_____ _(_)___  __  __/ __/___ ______/ /______
    \__ \/ / / / ___/ __/ __ '/ / __ \/ / / / /_/ __ '/ ___/ __/ ___/
   ___/ / /_/ (__  ) /_/ /_/ / / / / / /_/ / __/ /_/ / /__/ /_(__  )
  /____/\__,_/____/\__/\__,_/_/_/ /_/\__, /_/  \__,_/\___/\__/____/
                                    /____/
`, "'", "`")

	log = logger.Get("main")
)

// @tag.name company
func main() {
	fmt.Print(logo)
	logger.MustConfigure(logger.Config{Level: "debug", Env: "dev"}, Version)

	log.Info().Msgf("Starting Company Service, version %s", Version)

	err := runApplication()
	if err != nil {
		log.Fatal().Msgf("Fatal error while running the application: %v", err)
	}
}

type Configuration struct {
	MongoURI    string `env:"MONGO_URI"`
	APIURL      string `env:"API_URL"`
	UIURL       string `env:"UI_URL"`
	JwtCertsURL string `env:"JWT_CERTS_URL"`
	Database    string `env:"DATABASE,default=companiesdb"`
	NoAuth      bool   `env:"NO_AUTH"`
}

func runApplication() error {
	ctx := context.Background()
	var config Configuration

	// Read configuration from environment variables
	if err := envconfig.Process(ctx, &config); err != nil {
		log.Error().Err(err).Msg("Unable to process configuration")
		return err
	}

	closer := repository.MustConnectMongo(ctx, config.MongoURI)
	defer closer.Close()

	db := repository.GetDatabase(config.Database)
	router := restapi.Configure(config.APIURL, config.UIURL, config.JwtCertsURL, Version, config.NoAuth)
	broker := broker.NewFakeBroker()

	// Companies
	companyRepo := repository.NewCompaniesRepository(ctx, db, utils.GenerateID)
	companyService := company.NewService(&companyRepo, utils.Validate)
	companyapi.Configure(companyService, router)

	// Disclosures
	disclosureRepo := repository.NewDisclosuresRepository(ctx, db, utils.GenerateID)
	disclosureService := disclosure.NewService(&disclosureRepo, utils.Validate, broker)
	disclosureapi.Configure(disclosureService, router)

	// Data Models
	dataModelRepo := repository.NewDataModelRepository(ctx, db, utils.GenerateID)
	dataModelService := datamodel.NewService(&dataModelRepo, utils.Validate)
	datamodelapi.Configure(dataModelService, router)

	// Dataset Models
	datasetmodelRepo := repository.NewDatasetModelRepository(ctx, db, utils.GenerateID)
	datasetmodelService := datasetmodel.NewService(&datasetmodelRepo, utils.Validate, dataModelService.ValidateProperties, nil)
	datasetmodelapi.Configure(datasetmodelService, router)

	// Rating Models
	ratingModelRepo := repository.NewRatingModelRepository(ctx, db, utils.GenerateID)
	ratingModelService := ratingmodel.NewService(&ratingModelRepo, utils.Validate)
	ratingmodelapi.Configure(ratingModelService, router)

	// Facts
	factsRepo := repository.NewFactsRepository(ctx, db, utils.GenerateID)
	factService := facts.NewService(&factsRepo, utils.Validate, broker, disclosureService.GetPublishedByCompany)
	factsapi.Configure(factService, router)

	// Ratings
	ratingRepo := repository.NewRatingRepository(ctx, db, utils.GenerateID)
	getRatingModels := func() ([]ratingmodel.RatingModel, error) { return ratingModelService.GetAll(true) }
	ratingService := rating.NewService(&ratingRepo, broker, factService.GetByCompany, getRatingModels)
	ratingapi.Configure(ratingService, router)

	restapi.Start()

	log.Info().Msg("Job's done")
	return nil
}
